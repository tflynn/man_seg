/* Copyright 2015 Thomas Flynn */
/*This file is part of manseg.

    manseg is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    manseg is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with manseg.  If not, see <http://www.gnu.org/licenses/>.*/

#include <stdio.h>
#include <vector>
#include <assert.h>
#include <string>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>

#include "point.h"

using namespace std;

#ifndef __PTX__
#define __PTX__

#define PTX_MODE_BIN 0
#define PTX_MODE_ASCII 1

#define NBD_RAD_DEF 2

enum {
  PTX_COLOR,
  PTX_COLOR_LABEL,
  PTX_INTEN,
  PTX_LABEL
};

class ptx_master{
 protected:
  bool ascii;
  string file_name;
  int rows, cols;
  int start;
  FILE * f;
  vector<point *> pts;
  
 public: 

  static ptx_master * openptx(char * , char *lname = NULL);
  static int get_npts(const string &fname);
  static int get_npts(const char *fname);
 
  ptx_master(string fname, bool ascii_ = true);
  ptx_master();
  ~ptx_master();
  ptx_master(const ptx_master & cpyptx);

  virtual bool read_ptx(const char *fname);
  virtual void get_color(int i, unsigned char &r, unsigned char &g, unsigned char &b);
  virtual void get_color(int i,int j, unsigned char &r, unsigned char &g, unsigned char &b);
  virtual int write_ptx(const char *fname);
  virtual void write_ptx_bin(const char *fname);
  virtual void get_label(int, int, char &l){ l = -20;}

  bool read_ptx(const string &fname);
  void open_ptx_bin(const char *fname);
  void read_ptx_bin();
 
  int write_ptx(const string &fname);
  void write_ptx_bin(const string &fname);
  
  string get_fname() { return file_name; }
  int get_rows() { return rows; }
  int get_cols() { return cols; }
  void get_dim(int &nrows, int &ncols) const { nrows = rows; ncols = cols;}
  int get_npts() const { return rows*cols; }
  bool isascii(){ return ascii; }

  point * get_pt(int i);
  point * get_pt(int i, int j);
  void get_pt(int i, int j, double *&pt);
  const point * get_pt(int i) const;
  point * operator[](int i);
  const point * operator[] (int i) const;
  point * operator()(int r, int c);
 
  void get_nbd(int row, int col, int &sz, point **res, int NBD_RAD = NBD_RAD_DEF);
  void get_params(double &minp, double &dp, double &mint, double &dt);
  void cart_to_sphere(point *pt, double *r, double *t, double *p);

  template <class X> void each_point(void (*pt_fn)(point *, X), X u);
  template <class X> void each_point(void (*pt_fn)(point *, int, int, X), X u);

};

typedef ptx_master ptx;

template<class T=ptx>
  class ptx_collection{
  
  private:
  vector<T *> ptxs;
  
  point *get_pt(int i);
  int total_points;
  
  void open(const string dir,bool ascii);
  point * random_pt(int start);
  string thedir;
  vector<string> fnames;

  bool oneptx;
  void ensure_presence(int i);
public:
  static void getdir(const string dir, vector<string> &files);
  static string itoa(int value, int base=10);
  void delete_all();
  ptx_collection(const char *, bool ascii = true);
  ptx_collection();

  ptx_collection(const string dir, bool ascii = true);
  ~ptx_collection();
  void toggle_oneptx(bool newval);
  void add_ptx(const string &fname,bool ascii=true);
  void add_dir(const string dir, bool ascii=true);
  string get_fname(int i) { return ptxs[i]->get_fname(); }
  int get_npts();
  int get_nptxs() { return ptxs.size(); }
  int get_rows(int i);
  int get_cols(int i);
  T * get_ptx(int i);
  
  template <class X> void each_point(void (*pt_fn)(point *, X), X u);
 
  void set_dir(string newdir);
  void write_all();
  point * operator[](int i){return (i<total_points)? get_pt(i) : NULL; }
  point * operator()(int p, int r, int c);
  point * get_pt(int p, int r, int c);
  void get_color(int p, int row, int col, unsigned char &r, unsigned char &g, unsigned char &b){
    ptxs[p]->get_color(row,col,r,g,b);
  }

  void operator+=(const ptx_collection * ptxs);
  void random_unique_pts(vector<pair<point *, point *> > & pts, int npts);

};

int ptx_type(const char *fname);
bool file_exists(string);

class color_ptx : public ptx {
 private:
  vector<char *> colors;
 public:
  bool read_ptx(const char *);
  color_ptx() : ptx() {}
  color_ptx(const char *fname) { read_ptx(fname); }
  color_ptx(const string &fname, bool){ read_ptx(fname.c_str()); }
  
  void get_color(int i, unsigned char &r, unsigned char &g, unsigned char &b);
  void get_color(int i,int j, unsigned char &r, unsigned char &g, unsigned char &b);
  int write_ptx(const char *fname);
};

class inten_ptx : public ptx {
 protected:
  vector<float> inten;
 public:
  bool read_ptx(const char *);
  inten_ptx() : ptx() {}
  inten_ptx(const char *fname) {   read_ptx(fname);  }
  inten_ptx(const string &fname, bool)   {   read_ptx(fname.c_str());  }
  void get_color(int i, unsigned char &r, unsigned char &g, unsigned char &b);
  void get_color(int i,int j, unsigned char &r, unsigned char &g, unsigned char &b);
  int write_ptx(const char *fname);
  
};

class labelled_ptx : public inten_ptx {
  private: 
  vector<char> label;
  vector<char> cmplabel;
  public:
  bool read_ptx(const char * fname);

  labelled_ptx() : inten_ptx() {}

  labelled_ptx(const string & fname, bool){
    printf("in this const!!\n");
    label_name = ""; read_ptx(fname.c_str()); 
  }
  
  labelled_ptx(const string & fname,  const string & lname){
    label_name = lname;
    printf("label_name: %s\n",label_name.c_str());
    read_ptx(fname.c_str());
  }
  
  void compare(const char * fname);
  
  void get_label(int i, char &l);
  void get_label(int i, int j, char & l);
  string label_name;

  pair<char, char> getBoth(int i);
  void write_labels(const string &fname);
  void kill_label(int lbl);
  };

#endif
