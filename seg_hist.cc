/* Copyright 2011 Thomas Flynn */
/*This file is part of manseg.

    manseg is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    manseg is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with manseg.  If not, see <http://www.gnu.org/licenses/>.*/

#include "seg_hist.h"

seg seg_hist::undo(){
  seg ret;
  
  if(segments.size() > 0){
    ret = segments.back();
    segments.pop_back();
  }
  
  return ret;
}

void seg_hist::new_seg(void *p){
  if(segments.size() > 9)
    segments.pop_front();
  segments.push_back(seg(p));
}

void seg_hist::add_pt(int a_id, int a_cur, int a_old){
  segments.back().push_back(pt_id(a_id,a_cur,a_old));
}

void seg_hist::push_seg(seg & aseg){
  segments.push_back(aseg);
}
