/* Copyright 2013 Thomas Flynn */
/*This file is part of manseg.

    manseg is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    manseg is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with manseg.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef SOQTWORLDVIEWER
#define SOQTWORLDVIEWER

#include <Inventor/Qt/SoQt.h>
#include <Inventor/Qt/viewers/SoQtFullViewer.h>
#include <Inventor/SbLinear.h>
#include <Inventor/events/SoLocation2Event.h>
#include <Inventor/nodes/SoCamera.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/SoPickedPoint.h>
#include <Inventor/actions/SoRayPickAction.h>
#include <Inventor/Qt/SoQtCursor.h>
#include <QCursor>
#include <QWidget>

class SoQtWorldViewer: public SoQtViewer{

  typedef SoQtViewer inherited;
 public:
  /* SoQtWorldViewer():SoQtFullViewer(NULL,NULL,SbBool(true),
			     SoQtFullViewer::BUILD_NONE,SoQtViewer::BROWSER,
			     SbBool(true)){}*/
 SoQtWorldViewer():SoQtViewer(NULL,NULL,SbBool(true),
			      SoQtViewer::BROWSER,SbBool(true)){};
  
 private:
  SbVec2s ctr;
  bool ignore;
  bool nav;

  virtual SbBool processSoEvent(const SoEvent * const ev);

  virtual void sizeChanged(const SbVec2s & sz);
};






#endif
