#ifndef HEPVis_SoArrow_h
#define HEPVis_SoArrow_h

#include <Inventor/fields/SoSFVec3f.h>
#include <Inventor/fields/SoSFFloat.h>
#include <Inventor/fields/SoSFBool.h>
#include <Inventor/fields/SoSFNode.h>
#include <Inventor/nodes/SoShape.h>

class SoSFNode;
//! SoArrow - Inventor version of the Arrow Geant Geometry entity
/*!
 *                                                                            
 * Node:             SoArrow                                                    
 *                                                                            
 * Description:     Represents the Arrow Geant Geometry entity              
 *                                                                            
 * Author:            Joe Boudreau Nov 11 1996                                    
 *                                                                            
 * An arrow has a cylinder and also a conical tip.  It is suitable for
 * representing vectors and the like.
 *
 * Always use Inventor Fields. This allows Inventor to detect a change to
 * the data field and take the appropriate action; e.g., redraw the scene.
*/

/*Modified by Thomas Flynn 2011 */

class SoArrow:public SoShape {

  //
  //! This is required
  //
  SO_NODE_HEADER(SoArrow);
 
public:

  //
  //! The tip
  //
  SbVec3f tip;

  //
  //! The tail
  //
  SbVec3f tail;

  //
  //! size of the shaft
  //
  float size;

  
  //
  //! the alternate representation, required
  //
  SoSFNode  alternateRep;

  SoSFFloat cylLength;

  //
  //! Constructor, required
  //
  SoArrow(SbVec3f _tail, SbVec3f _tip, float _size);
  SoArrow();
  //
  //! Class Initializer, required
  //
  static void initClass();
  void updateBBox();
  //
  //! Generate AlternateRep, required.  Generating an alternate representation
  //! must be done upon users request.  It allows an Inventor program to read
  //! back the file without requiring *this* code to be dynamically linked. 
  //! If the users expects that *this* code will be dynamically linked, he
  //! need not invoke this method.  
  //
  virtual void generateAlternateRep();

  //
  //! We better be able to clear it, too!
  //
  virtual void clearAlternateRep();

  virtual void computeBBox(SoAction *action, SbBox3f &box, SbVec3f &center );

  //
  //! Generate Primitives, required
  //
  virtual void generatePrimitives(SoAction *action);

  //
  //! GLRender, required
  //
  virtual void GLRender(SoGLRenderAction *action); 

protected:

  SbBox3f _box;
  SbVec3f _center;

  //
  //! compute bounding Box, required
  //

  //
  //! GetChildList, required whenever the class has hidden children
  //
  virtual SoChildList *getChildren() const;

  //
  //! Destructor, required
  //
  virtual ~SoArrow();

private: 

  //
  //! Generate Children. Used to create the hidden children. Required whenever
  //! the node has hidden children.  
  //
  void generateChildren();  

  //
  //! Used to modify hidden children when a data field is changed. Required 
  //! whenever the class has hidden children. 
  //
  void updateChildren();

  //
  //! ChildList. Required whenever the class has hidden children.  
  //
  SoChildList *children;

  //
  //! Cache this locally; if it doesn't change, don't regenerate.
  //
  SbVec3f cachedTip;

  //
  //! Cache these locally; if it doesn't change, don't regenerate.
  //
  SbVec3f cachedTail;

  //
  //! Cache these locally; if it doesn't change, don't regenerate.
  //
  SoSFFloat cachedSize;

  SoSFFloat cachedCylLength;
  //
  //! Cache these locally; if it doesn't change, don't regenerate.
  //
  SoSFBool  cachedConicalShaft;
};

#endif
