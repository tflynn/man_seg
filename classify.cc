/* Copyright 2013 Thomas Flynn */
/*This file is part of manseg.

    manseg is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    manseg is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with manseg.  If not, see <http://www.gnu.org/licenses/>.*/

#include "classify.h"

classify::classify(const string &fn){
  loadClasses(fn);
}

classify::classify(){
}

void classify::loadClasses(const string & fn){
  FILE * f = fopen(fn.c_str(),"r");
  char name[255];
  int r,g,b,io_id;
  
  while(fscanf(f,"%s %d %d %d %d",name,&r,&g,&b,&io_id) != EOF){
    addClass(name,r,g,b,io_id);
    printf("Adding class Name: %s Color: (%d, %d, %d) ID: %d\n",name,r,g,b,io_id);
  }
}

void classify::addClass(char * name,unsigned char color[3], int id){
  addClass(string(name),color,id);
}

void classify::addClass(char * name,unsigned char r, unsigned char g, unsigned char b, int id){
  unsigned char color[3] = {r,g,b};
  addClass(name,color,id);
}

void classify::addClass(string name,unsigned char color[3], int id){
  classNames.push_back(name);
  unsigned char *nc = new unsigned char[3];
  nc[0] = color[0];
  nc[1] = color[1];
  nc[2] = color[2];
  classColors.push_back(nc);
  classIds.push_back(id);
}

void classify::getColorByIdx(int idx, unsigned char &r, unsigned char &g, unsigned char &b){
  r = classColors[idx][0];
  g = classColors[idx][1];
  b = classColors[idx][2];
}

void classify::getColorByIdx(int idx, float * ret0){
 ret0[0] = (float)classColors[idx][0]/255.;
 ret0[1] = (float)classColors[idx][1]/255.;
 ret0[2] = (float)classColors[idx][2]/255.;
}

void classify::getColorByName(string name, unsigned char * ret){
  int i=0;
  int sz = classNames.size();
  for(i=0;i<sz;i++)
    if (classNames[i] == name)
      for (int j =0 ;j<3;j++)
	ret[j] = classColors[i][j];
}

void classify::getColorByName(string name, float *ret){
  int i=0;
  int sz = classNames.size();
  
  for(i=0;i<sz;i++){
    if(classNames[i] == name){
      for(int j=0;j<3;j++)
	ret[j] = (float) classColors[i][j]/255.;
    }
  }
}

int classify::getIdByName(string name){
  int i=0;
  int sz = classNames.size();
  for(i=0;i<sz;i++)
    if(classNames[i] == name) return classIds[i];
  cout << "NO id found for NAME !! getIdByName" << endl;
}

int classify::getIdxByName(string name){
  int i=0;
  int sz = classNames.size();
  for(i=0;i<sz;i++)
    if(classNames[i] == name) return i;
  cout << "NO idx FOUND for name !! getIdxByName" << endl;
  return -1;
}

void classify::getClassByIdx(int idx, string &name, unsigned char &r, unsigned char &g, unsigned char &b){
  name = classNames[idx];
  r = classColors[idx][0];
  g = classColors[idx][1];
  b = classColors[idx][2];
}

void classify::getClassByIdx(int idx, string &name, float &r, float  &g, float &b){
  name = classNames[idx];
  r = (float)classColors[idx][0]/255.;
  g = (float)classColors[idx][1]/255.;
  b = (float)classColors[idx][2]/255.;
}

int classify::getIdxById(int id){
  int i=0;
  int sz = classNames.size();
  for(i=0;i<sz;i++)
    if(classIds[i] == id) return i;
  cout << "NO NAME FOUND!! getIdxById id was " << id << endl;
  return getIdxByName("none");
}

int classify::getIdByIdx(int idx){
  if(idx >= classIds.size() ) cout << "No such label idx: " << idx << endl;
  return classIds[idx];
}

string classify::getNameByIdx(int idx){
  return classNames[idx];
}
