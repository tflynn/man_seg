/* Copyright 2014 Thomas Flynn */
/*This file is part of manseg.

    manseg is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    manseg is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with manseg.  If not, see <http://www.gnu.org/licenses/>.*/

#include "coinwindow.h"


SoPath * CoinWindow::featCB(void * pData, const SoPickedPoint * pt){ //FEAT
  if(!pt) return NULL;
  if(!pt->getDetail()) return NULL;
  if(pt->getDetail()->getTypeId() != SoPointDetail::getClassTypeId())
    return NULL;

  CoinWindow * cw = (CoinWindow*) pData;
  SoPointDetail * det = (SoPointDetail *) pt->getDetail();
    
  int cur = det->getCoordinateIndex();
  printf("Clicked on %d\n", cur);
  int r,c,rt,ct,rows,cols;
  cw->temp->get_dim(rows,cols);
  cw->rf.getloc(cur,r,c);
  printf("Scanline: %d # %d \n",r,c);
  //  return NULL;

  cw->hist->new_seg(cw->pExtSelect);

  //  printf("Some other idxs on the horiz:\n");
  int sl=c;
  //  for(sl= max(0, c - 5); sl <= min(c+5, cols-1); sl++){
  for(sl = 0; sl < cols;sl++){ //< -- all horiz lines
    printf("Processing sl %d\n",sl);

  vector<float> evs;
  for(int i=0;i<rows;i++){
    cw->rf.getid(cur,i,sl);
    if(cur != -1){
      //      printf("Trying to label a valid point\n");
      cw->label_point(cur);
    }

    evs.push_back(cw->rf.evals(i,sl,1));
    // printf("%f\n",evs.back());
    /*  qprintf("%d \n",cur);
    cw->rf.getloc(cur,rt,ct);
    printf("Check: got %d %d\n",rt,ct);*/
  }
  plot(evs,false,sl);
  }
  return NULL;
}

/* 
CoinWindow::undoCallback(void *Data, soselection *){

This function is called at the beginning of every selection.
i.e. right before the points are actually colored this function is called.

data is a pointer to the coinwindow from which the callback was added.
We tell the cw->hist object that a new segment is being labelled (used for the undo functionality).
*/

void CoinWindow::startSelect(void *data, SoSelection *){
  printf("Started a sel\n");
  CoinWindow * cw = (CoinWindow *)data;
  cw->hist->new_seg(cw->pExtSelect);
}

/*
CoinWindow::undoCallback(void *userdata, SoEventCallback *node)
This callback provides the undo functionality. It is called whenever a "keyboard event"
happens (key is pressed or released).
We check to see if the Z was pressed, and also if ctrl was pressed.
If so we undo the last selection. userdata contains information we need to actually do the undo.
Namely, it is a pointer to the coinwindow class itself (from which the callback was added).
The cw->hist object is of type seg_hist (see seg_hist.cc). 
*/

void CoinWindow::undoCallback(void *userdata,SoEventCallback *node){
  const SoEvent *ev = node->getEvent();
  if(SoKeyboardEvent::isKeyPressEvent(ev,SoKeyboardEvent::Z) &&
     ev->wasCtrlDown() ){
    CoinWindow *cw = (CoinWindow *)userdata;
    seg s = cw->hist->undo();
    int npts = s.size();
    
    for(int i=0;i<npts;i++){
      pt_id cpt =  s.back();
      s.pop_back();
      cw->set->materialIndex.set1Value(cpt.id,cpt.old);
    }
  }
  
}

/* 
CoinWindow::initClasses()
SoQt needs to be initialized for some reason.
   Then our custom classes need to be registered with coin 
*/
void CoinWindow::initClasses(){

  SoQt::init(this);
  brushSelect::initClass();
  planeSelect::initClass();
  SoArrow::initClass();
  SoPenSelection::initClass();
  SoBaseSelection::initClass();
  SoKeySelect::initClass();
  SoIndexedNew::initClass();
  SoCursor::initClass();
}

/*
CoinWindow::makeSelectionBox() 
This sets up the window containing the tool buttons.
Also we set the callbacks for the buttons. the connect() statements
at the bottom tell Qt which function to call when a user clicks on a button
or chooses tool
*/

void CoinWindow::makeSelectionBox(){
  QWidget * toolbar = new QWidget;
 
  QVBoxLayout * vlay = new QVBoxLayout();
  QRadioButton * brushBtn = new QRadioButton(tr("Brush"));
  QRadioButton * regBtn = new QRadioButton(tr("Polygon"));
  QRadioButton * lassoBtn = new QRadioButton(tr("Lasso"));
  QRadioButton * rectBtn = new QRadioButton(tr("Rectangle"));
  QRadioButton * featBtn = new QRadioButton(tr("Features")); //FEAT
  
  planarCheck = new QCheckBox(tr("Planar"));

  QRadioButton * keyBtn = new QRadioButton(tr("Keyboard"));
  QRadioButton * boxBtn = new QRadioButton(tr("Box"));
  
  QPushButton * cmpBtn = new QPushButton(tr("Compare labels"));
  QPushButton * findEmptyBtn = new QPushButton(tr("Find unlabeled"));
  
  /*TypeList is a custom Qt Widget that lets the user select
    which labels to assign points (see typelist.cc)*/

  tl = new TypeList(cl);
  tl->setSizePolicy(QSizePolicy(QSizePolicy::Ignored,QSizePolicy::Ignored ) );

  setbox = new QGroupBox(tr("Settings"));

  QVBoxLayout * slay = new QVBoxLayout;


  setbox->setLayout(slay);
  setbox->setSizePolicy(QSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum ) );

  vlay->addWidget(brushBtn); 
  vlay->addWidget(regBtn);
  vlay->addWidget(rectBtn);
  vlay->addWidget(lassoBtn);
  vlay->addWidget(keyBtn);
  vlay->addWidget(featBtn);
  vlay->addWidget(planarCheck);
  //vlay->addWidget(boxBtn);
  vlay->addWidget(setbox);
  vlay->addWidget(tl);
 
  vlay->addWidget(cmpBtn);
  vlay->addWidget(findEmptyBtn);
  /*add the button callbacsk*/
  connect(brushBtn,SIGNAL(clicked()),SLOT(brushClicked()));

  connect(regBtn,SIGNAL(clicked()),SLOT(polyClicked()));
  connect(lassoBtn,SIGNAL(clicked()),SLOT(lassoClicked()));
  connect(keyBtn,SIGNAL(clicked()),SLOT(keyClicked()));
  connect(rectBtn,SIGNAL(clicked()),SLOT(rectClicked()));
  //connect(boxBtn,SIGNAL(clicked()),SLOT(boxClicked()));
  connect(featBtn,SIGNAL(clicked()),SLOT(featClicked())); //FEAT
  connect(cmpBtn,SIGNAL(clicked()),SLOT(compareClicked()));
  connect(findEmptyBtn,SIGNAL(clicked()),SLOT(findEmpty()));
 
  toolbar->setLayout(vlay);
  toolbar->resize(200,700);

  toolbar->show();

}



/*CoinWindow::makeViewWind()

This function makes the coin window (soqtexaminerviewer) which displays the point cloud the 
user is labelling. We also add savelabel button, and register the undo callback function.
Note that this keyboard callback is registered with coin, unlike the button callback
which is registered with Qt.
After this function is called the scenegraph looks like:
  SoSep
  /  \
SoEV SoSel

Where SoEV is a soeventcallback node and
SoSel is a soselection node.
The SoSeparator doesn't do anything, just contains children.
(See coin doc). (We could easily use a SoGroup node here instead)
*/

void CoinWindow::makeViewWind(){
  char fname[255];
  viewer =new hotviewer;
  viewer->setBackgroundColor(SbColor(200./255,200./255,200./255));

  SoSeparator * root = new SoSeparator;
  SoEventCallback * scb = new SoEventCallback;

  pExtSelect = new brushSelect(this);
  root->addChild(new SoPerspectiveCamera);
  root->addChild(pExtSelect);
  root->addChild(scb);
  scb->addEventCallback(SoKeyboardEvent::getClassTypeId(),undoCallback,this);
 

  SoPerspectiveCamera * pcam = new SoPerspectiveCamera;
  pcam->position = SbVec3f(0,0,5);
  pcam->nearDistance = 0.1;
  pcam->farDistance = 10;
  
  root->addChild(pcam);

  curs = new SoCursor;
  curs->rad.setValue(0.01);
  root->addChild(curs);
  // viewer->setOverlaySceneGraph(curs);

 viewer->setSceneGraph(root);
 viewer->setAutoRedraw(true);

  QVBoxLayout * vLay = new QVBoxLayout(this);
  saveLabel = new QPushButton(tr("Save labels"));
  vLay->addWidget(viewer->getWidget());
 
  QPushButton * saveOnlyLabel = new QPushButton(tr("Save current label"));
  vLay->addWidget(saveLabel);
  vLay->addWidget(saveOnlyLabel);
  connect(saveLabel,SIGNAL(clicked()),SLOT(saveClicked()));
  connect(saveOnlyLabel,SIGNAL(clicked()),SLOT(saveOnlyClicked()));
}

/*
CoinWindow::CoinWindow(classify *cl, QWidget *parent)
The constructor for coinwindow. cl is a pointer to a classify object, containing
info about the labels (correspondence between names, colors, and color ids).
parent defaults to null (parent is used by Qt internally. If it's null then
CoinWindow gets its own window). 
RestList is used for choosing which labels to ignore,
hist is used for undo (see seg_hist.cc, restlist.cc)

*/
CoinWindow::CoinWindow(classify * _cl, QWidget * parent) : QWidget(parent) {
  initClasses();

  planeSel = NULL;
  cl = _cl;
 
 
  hist = new seg_hist;


  makeSelectionBox();
  makeViewWind();
}

/*CoinWindow::compareClicked()
callback for compare button.
used to compare two labellings of the same ptx*/

void CoinWindow::compareClicked(){

  QString lbl_name = QFileDialog::getOpenFileName(this,tr("Open labels"));

  if(lbl_name.isEmpty()) return;

  string lblfname = lbl_name.toUtf8().constData();
 
  if(!dynamic_cast<labelled_ptx *> (temp)) return;
  
  labelled_ptx * lptx = (labelled_ptx *) temp;

  lptx->compare(lblfname.c_str());
 
  CmpTable * ct = new CmpTable(cl,this);
  ct->setPtx(lptx,&(set->visIndex));
  
}

void CoinWindow::findEmpty(){
  int noneid = cl->getIdxByName("none");
  int npts = set->materialIndex.getNum();
  int i;
  for(i=0;i<npts;i++)
    if(set->materialIndex[i] == noneid)
      viewer->getCamera()->position.setValue(pts->point[i]);
}


/*
CoinWindow::replaceSelection(SoSeparator *rs)

This function is called from each of the tool button callbacks.
i.e. whenever the user chooses a new selection tool.
rs is a pointer to the new selection tool (We accept SoSeparator arguments since all the selections
inherit from SoSeparator).

As described in makeViewWind, the root of the scenegraph contains two children:
one responsible for the keyboard callback (undo) and another responsible for selection (which inherits from SoSeparator).
We wish to replace the selection node with our new node (rs).
However, under the selection node is the actual point cloud and the coloring info which we want to keep (coinwindow.sep)
So we add sep to rs, and replace the old selection with rs.
pExtSelect always points to the current selection node.
*/

void CoinWindow::replaceSelection(SoGroup * rs){
  printf("In replace\n");
  SoGroup * root = (SoGroup *)viewer->getSceneGraph();
  int idx;

  if( planeSel == NULL){
    printf("In planesel\n");
    idx = root->findChild(pExtSelect);
  }
  else{
    printf("In root find child\n");
    idx = root->findChild(planeSel);
  }
  SoChildList * cl = pExtSelect->getChildren();

  
  // rs->ref();
  pExtSelect = rs;
  if(planarCheck->isChecked()){
    printf("The planar is checked\n");
    rs = new SoBaseSelection(rs);
    planeSel = (SoBaseSelection *)rs;
    makePlanarSettings();
  }
  else{
    printf("The planar is not checked\n");
    planeSel = NULL;
  }
  printf("Done with replace\n");
  rs->addChild(sep);

  cl->truncate(0);
 
  
  root->replaceChild(idx,rs);
  printf("Replaced\n");
}

/*
CoinWindow::clearSettingsBox()
We call this when a new selection tool is chosen. It clears the settings box of the old selection
to make room for the options of this selection.
*/

void CoinWindow::clearSettingsBox(){
  QVBoxLayout * slay = (QVBoxLayout *) setbox->layout();
  int cnt = slay->count();
  int i;

  if(cnt > 0){ 
    QLayoutItem * itm;
    for(i=cnt-1;i>=0;i--){
      itm = slay->takeAt(i);
      delete itm->widget();
    }
  }
  
}


void CoinWindow::lassoClicked(){
  printf("entering lasso mode\n");
  clearSettingsBox();
  SoPenSelection * ps = new SoPenSelection;
 
  replaceSelection(ps);

  ps->setPointFilterCallback(CoinWindow::pointCB,this);
  ps->addStartCallback(CoinWindow::startSelect,this);
}


void CoinWindow::polyClicked(){
  printf("entering polyon mode\n");

  clearSettingsBox();
 
  SoExtSelection * es = new SoExtSelection;
 
  es->lassoType = SoExtSelection::LASSO;
  es->lassoPolicy = SoExtSelection::PART;
  es->lassoMode = SoExtSelection::ALL_SHAPES;
  es->setPointFilterCallback(CoinWindow::pointCB,this);
  es->addStartCallback(CoinWindow::startSelect,this);
 
 replaceSelection(es);

}

void CoinWindow::featClicked(){
  printf("Entering feat mode\n");
  clearSettingsBox();
  SoSelection * sl = new SoSelection;
  sl->setPickFilterCallback(CoinWindow::featCB,this);
  sl->addStartCallback(CoinWindow::startSelect,this);

  replaceSelection(sl);
}

void CoinWindow::rectClicked(){

  printf("entering rect mode\n");

  clearSettingsBox();
 
  SoExtSelection * es = new SoExtSelection;
 
  es->lassoType = SoExtSelection::RECTANGLE;

  es->lassoPolicy = SoExtSelection::PART;
  es->lassoMode = SoExtSelection::ALL_SHAPES;
  es->setPointFilterCallback(CoinWindow::pointCB,this);
  es->addStartCallback(CoinWindow::startSelect,this);
 
 replaceSelection(es);


}

void CoinWindow::brushClicked(){
  printf("entering brush mode\n");
  clearSettingsBox();
  makeBrushSettings();

  brushSelect * bs = new brushSelect(this);
  curs->rad.connectFrom(&bs->rad);

  replaceSelection(bs);

}

void CoinWindow::boxClicked(){
  printf("entering box mode\n");
  clearSettingsBox();
  SoGroup * gp = new SoGroup;
  SoTransformBoxDragger * dragger = new SoTransformBoxDragger;
  gp->addChild(dragger);
  replaceSelection(gp);

}

void CoinWindow::keyClicked(){
  clearSettingsBox();
  printf("entering keyboard mode\n");
  replaceSelection(new SoKeySelect(this));
}

/*Callback from when the user modifies the threshold of the distance
to plane, for the planar selector. We need to go back to the last
segment and reevaluate all the distances.
*/
void CoinWindow::planarSlide(double d){
 
  
  //planeSelect  * ps = ((SoBaseSelection *) pExtSelect)->ps;
  printf("new dist: %f\n",d);
  planeSel->ps->setDist( d );

  redoLastSegment();
}

/*
CoinWindow::redoLastSegment(){
  Here take the last segment and reevaluate
  each point to see if its within the new distance to the plane.
*/
void CoinWindow::redoLastSegment(){
 
  seg s = hist->undo();

  if(s.ptr != pExtSelect){
    hist->push_seg(s);
    return;
  }

  seg::iterator it;
  for(it = s.begin() ;it != s.end();it++){
    pt_id cpt = *it;
    if(! planeSel->validPt(pts->point[cpt.id]))
      set->materialIndex.set1Value(cpt.id,cpt.old);
    else
      set->materialIndex.set1Value(cpt.id,cpt.cur);
    
  }
  hist->push_seg(s);

}

/*
CoinWindow::brushSlide
control brush width
*/

void CoinWindow::brushSlide(){
  int p = slide->sliderPosition();
  printf("brush pos: %d\n",p);
  brushSelect * bs = (brushSelect *) pExtSelect;
  bs->setRad(p);
}

void CoinWindow::makePlanarSettings(){
  printf("Making planar settings\n");
  QVBoxLayout * slay = (QVBoxLayout *) setbox->layout();

  QLabel * l = new QLabel(tr("Max Distance"));
  l->setSizePolicy(QSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum ) );
  sbox = new QDoubleSpinBox;
  sbox->setMaximum(100000);
  sbox->setSingleStep(0.1);
  sbox->setValue(0.1);
  /*  slide = new QSlider(Qt::Horizontal);
  slide->setMaximum(100);
  slide->setMinimum(0);*/

  //  QPushButton * p_btn = new QPushButton("Add plane");
  QPushButton * e_btn = new QPushButton("Save heights");
  
  invert = new QCheckBox(tr("Invert"));

  invert->setChecked(false);
  
  slay->addWidget(l);
  slay->addWidget(sbox);
  slay->addWidget(invert);
  //  slay->addWidget(btn);
  slay->addWidget(e_btn);
  connect(sbox,SIGNAL(valueChanged(double)),SLOT(planarSlide(double)));
  connect(invert,SIGNAL(stateChanged(int)),SLOT(invertBox(int)));
  connect(e_btn,SIGNAL(clicked()),SLOT(elevationClicked()));

  // slay->setSizePolicy(QSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum ) );
  slay->setSizeConstraint(QLayout::SetMinimumSize);
}

/*
CoinWindow::invertBox(int state)
Called when user chooses the invert option of the planar selection.
We need to redo the last segment, as is the case when the user uses the distance slider.
*/

void CoinWindow::invertBox(int state){

  planeSel->ps->invert = (state == Qt::Checked);
  
  redoLastSegment();

}

void CoinWindow::makeBrushSettings(){
  QVBoxLayout * slay = (QVBoxLayout *) setbox->layout();

  QLabel * l = new QLabel(tr("Brush Size"));
  slide = new QSlider(Qt::Horizontal);
  slide->setMaximum(100);
  slide->setMinimum(0);

  slay->addWidget(l);
  slay->addWidget(slide);
  connect(slide,SIGNAL(sliderReleased()),SLOT(brushSlide()));

}

/*
saveClicked()
The user wants to save the labelling. For each valid (non zero) point 
in the ptx we look up the corresponding color of that point in the point cloud.
The color is mapped to the label id via our classify object cl.
*/

void CoinWindow::saveClicked(){
  QString lbl_name = QFileDialog::getSaveFileName(this,tr("Save labels"));

  if(lbl_name.isEmpty()) return;

  string lblfname = lbl_name.toUtf8().constData();
  
  FILE * f_lbl = fopen(lblfname.c_str(),"w");
  int npts = temp->get_npts();
  
  fprintf(f_lbl,"0\n");

  int cur_val=0;
  point *cpt;
  int lbl;
  int i;

  for(i=0;i<npts;i++){
    cpt = temp->get_pt(i);
    if(cpt->valid == true){
      lbl = cl->getIdByIdx(set->materialIndex[cur_val]);
      cur_val++;
    }
    else{
      lbl = -20;
    }
    
    fprintf(f_lbl,"%d 0\n",lbl);
  }

  fclose(f_lbl);
  
}

void CoinWindow::saveOnlyClicked(){
  QString lbl_name = QFileDialog::getSaveFileName(this,tr("Save labels"));

  if(lbl_name.isEmpty()) return;

  string lblfname = lbl_name.toUtf8().constData();
  
  FILE * f_lbl = fopen(lblfname.c_str(),"w");
  int npts = temp->get_npts();
  
  fprintf(f_lbl,"0\n");

  int cur_val=0;
  point *cpt;
  int lbl;
  int i;
  int ci;
  for(i=0;i<npts;i++){
    cpt = temp->get_pt(i);
    if(cpt->valid == true){
      ci = set->materialIndex[cur_val];
      if(ci != tl->getType())
	lbl = -20;
      else
	lbl = cl->getIdByIdx(set->materialIndex[cur_val]);
      
      cur_val++;
    }
    else{
      lbl = -20;
    }
    
    fprintf(f_lbl,"%d 0\n",lbl);
  }

  fclose(f_lbl);
  
}

void CoinWindow::elevationClicked(){
  printf("cool\n");
 
  int i;
  int npts = temp->get_npts();
  point * p_pt; //ptx point
  SbVec3f c_pt; //coin point
  float height;
  

  QString em_name = QFileDialog::getSaveFileName(this,tr("Save elevation map"));

  if(em_name.isEmpty()) return;

  string emfname = em_name.toUtf8().constData();
  

  FILE * f_emap = fopen(emfname.c_str(),"w");

  for(i=0;i<npts;i++){
    p_pt = temp->get_pt(i);
    if(p_pt->valid == true){
      c_pt[0] = p_pt->c[0];
      c_pt[1] = p_pt->c[1];
      c_pt[2] = p_pt->c[2];
      
      height = planeSel->getDist(c_pt);
    }
    else{
      height = -1;
    }
    fprintf(f_emap,"%f\n",height);
  }

  printf("saved!\n");
  fclose(f_emap);
}

/*
CoinWindow::setFile(char *file, char *lname)
file is the ptx file, lname is the label file.
The ptx class temp (bad name!) stores the correspondence between points and colors.
We then add the points, with colors defined by the labels, using the ptx_to_sep function.
We then add this separator right beneath our selection node.
*/

void CoinWindow::setFile(char * file, char *lname){
  temp = ptx::openptx(file,lname);
  sep = ptx_to_sep(temp);
  pExtSelect->addChild(sep);
  viewer->viewAll();
  setWindowTitle(tr(file));
}

/*
CoinWindow::label_point(int coordIndex)
This function is called by every selection tool when a point is chosen.
We check that the current label of the coordindex is not in our ignore list.
(rl->badType). Then we check to see if the point is currently being rendered.
(set->visIndex).
Then if we are currently using a plane selection
(SoBaseSelection) we need to do the planar test as well.
If we pass these tests then we change the color and
add the point to hist (for undo functionality).

*/

SbBool CoinWindow::label_point(int coordIndex){
 
  int oldVal = set->materialIndex[coordIndex];
  int newVal = tl->getType();
  
  if(tl->badType(oldVal) ) return FALSE;

  if(set->visIndex[coordIndex] == false) return FALSE;
  //  printf("in lbl\n");

  if(planarCheck->isChecked()){
 
    // printf("yaplanar\n");
    if( ! planeSel->validPt(pts->point[coordIndex])){
      hist->add_pt(coordIndex,newVal,oldVal);
      return FALSE;
    }
  }
 
  set->materialIndex.set1Value(coordIndex,newVal);
  
  hist->add_pt(coordIndex,newVal,oldVal);
  return FALSE;
}

/*
CoinWindow::pointCB
Some selections call this function instead of label point.
This function gets raw coin data from the selection
and extracts the interesting info (coordIndex)
and then calls label point. 
*/
SbBool CoinWindow::pointCB( void *pData, 
                SoCallbackAction *, 
                const SoPrimitiveVertex *pVertex ) 
{
  CoinWindow * cw = (CoinWindow *)pData;
  SoPointDetail *pDetail = (SoPointDetail*)pVertex->getDetail();
 
  if(!pDetail) return FALSE;

  int coordIndex = pDetail->getCoordinateIndex();
 
  return cw->label_point(coordIndex);

}


SoSeparator * CoinWindow::ptx_to_sep(ptx *aptx){

  SoSeparator *root = new SoSeparator;
  SoDrawStyle *draw = new SoDrawStyle;
  mat = new SoMaterial;
  SoMaterialBinding * matb = new SoMaterialBinding;
  pts = new SoCoordinate3;
  set = new SoIndexedNew;
    
  double * pt;
  float clr[3];

  int sz = cl->getNClasses();
  int cur = 0;

  int rows,cols,i,j,lbl_clr;

  char lbl;
  
  for(i=0;i<sz;i++){
    cl->getColorByIdx(i,clr);
    mat->diffuseColor.set1Value(i,clr);
  }

  clr[0] = 0.5;
  clr[1] = 0.2;
  clr[2] = 0.1;

  mat->diffuseColor.set1Value(i,clr);

  aptx->get_dim(rows,cols);
  
  rf.resize(rows,cols); //ROWFEATS
  rf.cw = this; //ROWFEATS;

  for(j=0;j<rows;j+=1){
    for(i=0;i<cols;i+=1){ //subsample
      
      aptx->get_pt(j,i,pt);
      aptx->get_label(j,i,lbl);
      
      if(pt != NULL) {
	lbl_clr=cl->getIdxById(lbl);
	if(lbl_clr == -1) continue;


	pts->point.set1Value(cur, 
			     (float)pt[0],(float)pt[1],(float)pt[2]);
	//(float)pt[0], (float)pt[1], 0);
	rf.addpt(j,i); //ROWFEATS
	set->visIndex.set1Value(cur,true);
	set->materialIndex.set1Value(cur,lbl_clr);
	set->coordIndex.set1Value(cur,cur);
	cur++;
      }

    }
  }
  printf("Showing %d pts\n",cur);
  num_val_pts = cur;
  matb->value = SoMaterialBinding::PER_VERTEX_INDEXED;
  draw->style=SoDrawStyle::POINTS;
  draw->pointSize=3.0;
  root->addChild(mat);
  root->addChild(matb); 
  root->addChild(draw);
  root->addChild(pts);
  root->addChild(set);

  return root;
}

void CoinWindow::view_line(char *fname){
  printf("Will show lines\n");
  FILE * f = fopen(fname,"r");
  // int start,stop;
  SoIndexedLineSet * solines = new SoIndexedLineSet;
  int pos=0;
  int total,thissz,nb;
  fscanf(f,"%d",&total);
  int i,j;
  bool will_draw=true;
  for(i=0;i<total;i++){
  
    fscanf(f,"%d",&thissz);
    will_draw = (rand() % 100)<100;
    for(j=0;j<thissz;j++){
      fscanf(f,"%d",&nb);
      if (not will_draw) continue;
      solines->coordIndex.set1Value(pos++,i);
      solines->coordIndex.set1Value(pos++,nb);
      
      solines->coordIndex.set1Value(pos++,-1);
    }
  }
  /*
  while(fscanf(f,"%d %d",&start,&stop) != EOF){
    solines->coordIndex.set1Value(pos++,start);
    solines->coordIndex.set1Value(pos++,stop);
    solines->coordIndex.set1Value(pos++,-1);
    }*/

  SoMaterialBinding * matb = new SoMaterialBinding;
  matb->value = SoMaterialBinding::NONE;
 
  SoDrawStyle *draw2 = new SoDrawStyle;
  draw2->style=SoDrawStyle::LINES;
  sep->addChild(matb);
  sep->addChild(draw2);
  sep->addChild(solines);
}
