/* Copyright 2013 Thomas Flynn */
/*This file is part of manseg.

    manseg is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    manseg is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with manseg.  If not, see <http://www.gnu.org/licenses/>.*/

#include "SoBaseSelection.h"
#include "planeselect.h"

SO_NODE_SOURCE(SoBaseSelection);

void SoBaseSelection::initClass(){
  SO_NODE_INIT_CLASS(SoBaseSelection, SoSeparator, "Separator");
}

SoBaseSelection::SoBaseSelection(SoGroup * _ss):ss(_ss){
  SO_NODE_CONSTRUCTOR(SoBaseSelection);
  ps = new planeSelect;
  ps->setDone(doneCB,this);
  ps->ref();
  addChild(ps);
  pses.push_back(ps);
  cur = ps;
  //ps->pickCBData = NULL;
  // printf("ss data: %d\n",this->pickCBData);
}

SbBool SoBaseSelection::validPt(SbVec3f v){
  bool val = false;
  int cnt = pses.size();
  int i;

  for(i=0;i<cnt;i++)
    if(pses[i]->validPt(v) ) return true;

  return false;

}

float SoBaseSelection::getDist(SbVec3f v){
  int cnt =pses.size();
  int i;
  float d = pses[0]->getDist(v);
  
  if (cnt == 1) return d;
  
  for(i=1;i<cnt;i++){
    d = fminf(d,pses[i]->getDist(v));
  }

  return d;
}
int SoBaseSelection::mode(){
  if(cur == ps){
    return CHOOSING_PLANE;
  }
  printf("CHOOSING POINTS\n");
  return CHOOSING_POINTS;
}

void SoBaseSelection::swap(){
  if (cur == ss){
    replaceChild(ss,ps);
    cur = ps;
  }
  else{
    SoChildList * cl = ps->getChildren();
    int n = cl->getLength();
    int i;

    for(i=0;i<n;i++){
      addChild((SoNode *)cl->get(i));
    }
    replaceChild(ps,ss);
    cur = ss;
  }
}

void SoBaseSelection::doneCB(void *_bs){
  ((SoBaseSelection * ) _bs)->swap();
}


