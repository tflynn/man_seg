/* Copyright 2014 Thomas Flynn */
/*This file is part of manseg.

    manseg is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    manseg is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with manseg.  If not, see <http://www.gnu.org/licenses/>.*/

#include "brushselect.h"
#include "coinwindow.h"

SO_NODE_SOURCE(brushSelect);

void brushSelect::initClass(){
  SO_NODE_INIT_CLASS(brushSelect, SoSelection, 
		     "Selection");
}

brushSelect::brushSelect(CoinWindow * _cw){
  SO_NODE_CONSTRUCTOR(brushSelect);
  cw = _cw;
  SoEventCallback * evCB = new SoEventCallback;
  down = false;
  evCB->addEventCallback
(SoMouseButtonEvent::getClassTypeId(), mouseButtonCB, this);
  raypick = new SoRayPickAction(cw->viewer->getViewportRegion());
  raypick->setPickAll(true);
  raypick->setRadius(10.);
  root = cw->viewer->getSceneManager()->getSceneGraph();
  children = new SoChildList(this);
  children->append(evCB);
  SO_NODE_ADD_FIELD(rad, (0));
}

void brushSelect::handleEvent(SoHandleEventAction *action){
  children->traverse(action);
}

void brushSelect::setRad(int _rad){
  rad.setValue((float)_rad);
  raypick->setRadius(rad.getValue());
  }


SoChildList * brushSelect::getChildren(){
  return children;
}

void brushSelect::GLRender(SoGLRenderAction *action){
  printf("YoD!\n");

}
void brushSelect::mouseButtonCB(void * data, SoEventCallback * evCB){
  const SoEvent * ev = evCB->getEvent();
  brushSelect * bs = (brushSelect *)data;
  if(SoMouseButtonEvent::isButtonPressEvent(ev,SoMouseButtonEvent::BUTTON1)){
    // printf("left mouse clicked!\n");
    bs->down=true;
    CoinWindow::startSelect(bs->cw,NULL);
    location2CB(data,evCB);
    evCB->addEventCallback(SoLocation2Event::getClassTypeId(),location2CB,bs);
    
  }
  else if (SoMouseButtonEvent::isButtonReleaseEvent(ev,SoMouseButtonEvent::BUTTON1)){
    bs->down=false;
    printf("left mouse released!\n");
    evCB->removeEventCallback(SoLocation2Event::getClassTypeId(),location2CB,bs);
  }

}

void brushSelect::location2CB(void *data, SoEventCallback * eventCB){
  const SoEvent * ev = eventCB->getEvent();

  brushSelect * bs = (brushSelect *)data;
 
  SoPickedPoint * tmp;
  SoPointDetail * pdet;

  bs->raypick->setPoint(ev->getPosition());

  bs->raypick->apply(bs->root);
  const SoPickedPointList pts = bs->raypick->getPickedPointList();
  int npts = pts.getLength();
  int i;
  printf("npts: %d\n",npts);

  // SoPrimitiveVertex pv;
  // SoPointDetail * pd;
  for(i=0;i<npts;i++){
    tmp = pts[i];
    
    // pd = (SoPointDetail*)tmp->getDetail()->copy();
    // pv.setDetail(pd);

    pdet = (SoPointDetail *)tmp->getDetail();
    bs->cw->label_point(pdet->getCoordinateIndex());
  }
  
}

