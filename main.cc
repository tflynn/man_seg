/* Copyright 2014 Thomas Flynn */
/*This file is part of manseg.

    manseg is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    manseg is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with manseg.  If not, see <http://www.gnu.org/licenses/>.*/

#include <iostream>
#include <QApplication>
#include "coinwindow.h"
#include <QGLFormat>
#include <QGLWidget>
using namespace std;

int 
main(int argc, char **argv){
  QApplication app(argc, argv);

  /* QGLFormat fmt;
 fmt.setOverlay(true);
 QGLWidget* myWidget = new QGLWidget(fmt);
 if (!myWidget->format().hasOverlay()) {
   printf("Could not enable overlay\n");
   }*/

  if(argc < 2){
    printf("Usage: %s a.ptx classes [a.lbl]\n",argv[0]);
  }

  classify cl(argv[2]);
  if(cl.getIdxByName("none") == -1)
    cl.addClass("none",160,32,240,-20);

  CoinWindow * cw = new CoinWindow(&cl);

  if(argc > 3){
    cout << "going to use label file " << string(argv[3]) << endl;
    cw->setFile(argv[1],argv[3]);
  }
  else{
    cw->setFile(argv[1]);
  }
  if(argc == 5)
    cw->view_line(argv[4]);
  SoQt::show(cw);
  SoQt::mainLoop();
  
  return 0;
}
