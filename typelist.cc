/* Copyright 2013 Thomas Flynn */
/*This file is part of manseg.

    manseg is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    manseg is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with manseg.  If not, see <http://www.gnu.org/licenses/>.*/

#include "typelist.h"

TypeList::TypeList(classify * _cl,QWidget * parent) : QWidget(parent) {
  cl = _cl;
  
  QGroupBox * gb = new QGroupBox(tr("Labels"),this);
  QVBoxLayout * lay = new QVBoxLayout;

  _list = new QListWidget();
  QListWidgetItem * test;
  lay->addWidget(_list);
  gb->setLayout(lay);

  int ntypes = cl->getNClasses();

  ignlist.resize(ntypes);

  int i;
  unsigned char r,g,b;
  string name;
  
  for(i=0;i<ntypes;i++){
    cl->getClassByIdx(i,name,r,g,b);
    test = new QListWidgetItem(name.c_str());
    test->setBackground(QBrush(QColor(r,g,b)));
    test->setCheckState(Qt::Unchecked);
    _list->addItem(test);
    ignlist[i] = false;
  }

  _list->setCurrentRow(cl->getIdxByName("none"));
  
  type = cl->getIdxByName("none");
  connect( _list, SIGNAL(itemChanged(QListWidgetItem *)),SLOT(itemChanged(QListWidgetItem *)));
  connect( _list, SIGNAL(itemClicked(QListWidgetItem *)),SLOT(itemClicked(QListWidgetItem *)));
}

int TypeList::getType(){
  return type;
}

bool TypeList::badType(int i){
  return ignlist[i];
}

void TypeList::itemClicked(QListWidgetItem *itm){
    type=cl->getIdxByName(itm->text().toStdString());
}

void TypeList::itemChanged(QListWidgetItem * itm){
  
  if(itm->checkState() == Qt::Unchecked){ 
    //   itm->setCheckState(Qt::Checked);
    ignlist[ _list->row(itm)] = false;
    printf("not ignoring\n");
  }
  else{
    printf("ignoring\n");
    // itm->setCheckState(Qt::Unchecked);
    ignlist[ _list->row(itm) ] = true;
  }
}
