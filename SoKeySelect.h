/* Copyright 2013 Thomas Flynn */
/*This file is part of manseg.

    manseg is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    manseg is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with manseg.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef __SOKEYSELECTION
#define __SOKEYSELECTION

#include <Inventor/nodes/SoSubNode.h>
#include <Inventor/nodes/SoSelection.h>
#include <Inventor/nodes/SoExtSelection.h>
#include <Inventor/events/SoEvent.h>
#include <Inventor/events/SoMouseButtonEvent.h> 
#include <Inventor/events/SoLocation2Event.h> 
#include <Inventor/fields/SoSFEnum.h>
#include <Inventor/actions/SoHandleEventAction.h>
#include <Inventor/SbLinear.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/events/SoKeyboardEvent.h>
#include <Inventor/details/SoPointDetail.h>
#include <Inventor/events/SoMouseButtonEvent.h>

class CoinWindow;
class SoKeySelect : public SoSeparator {

  SO_NODE_HEADER(SoKeySelect);

public:
  int cur;
  SoKeySelect(CoinWindow * _cw = NULL);
  virtual void handleEvent(SoHandleEventAction *node);
  void keyEvent(const SoKeyboardEvent * ev);
  void mouseEvent(SoHandleEventAction * node);
  static void initClass();
  SoCoordinate3 * scd;
  SoMaterial * mat;
  SoTranslation * tra;
  SoSphere * sph;
  CoinWindow * cw;
};

#endif
