/* Copyright 2011 Thomas Flynn */
/*This file is part of manseg.

    manseg is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    manseg is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with manseg.  If not, see <http://www.gnu.org/licenses/>.*/

#include <QWidget>
#include <QListWidget>
#include <QListWidgetItem>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QStringList>
#include <QPushButton>
#include "ptx.h"

#include <Inventor/fields/SoMFInt32.h>
#include <Inventor/fields/SoMFBool.h>

#include <iostream>
#include "classify.h"
#include <stdio.h>
using namespace std;

typedef pair<char,char> lpair;

#ifndef CMPTABLE
#define CMPTABLE

class CmpTable : public QWidget
{
  Q_OBJECT
 public:
  CmpTable(classify * _cl, QWidget * parent = 0);
  QStringList getLabels();
  void setPtx(labelled_ptx * aptx, SoMFBool * visI);
  void toggle_pts(int r, int c, bool show);
 private slots:
  void cellChanged(int, int);
  void showAll();

 private:
  SoMFBool * visIndex;
  labelled_ptx * aptx;

  QTableWidget * qtw;
  int type;
  classify * cl;
  QListWidget * _list;
  vector<bool> ignlist;
};

#endif
