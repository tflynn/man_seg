/* Copyright 2013 Thomas Flynn */
/*This file is part of manseg.

    manseg is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    manseg is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with manseg.  If not, see <http://www.gnu.org/licenses/>.*/

#include "SoKeySelect.h"
#include "coinwindow.h"

SO_NODE_SOURCE(SoKeySelect);

void SoKeySelect::initClass(){
  SO_NODE_INIT_CLASS(SoKeySelect, SoSeparator, "Separator");
}

SoKeySelect::SoKeySelect(CoinWindow * _cw): cw(_cw){
  SO_NODE_CONSTRUCTOR(SoKeySelect);
  cur = 0;
  scd = cw->pts;
  SoDrawStyle * ds = new SoDrawStyle;
  sph = new SoSphere;
  mat = new SoMaterial;
  tra = new SoTranslation;
  ds->style = SoDrawStyle::LINES;
  sph->radius = 0.1;
  SoSeparator * sep = new SoSeparator;
  sep->addChild(tra); 
  sep->addChild(mat);
  sep->addChild(ds);
  sep->addChild(sph);
  addChild(sep);
}

void SoKeySelect::keyEvent(const SoKeyboardEvent * ev){

  if(SoKeyboardEvent::isKeyReleaseEvent(ev,SoKeyboardEvent::ANY))
    return;

  switch (ev->getKey() ){
  case SoKeyboardEvent::PAGE_UP:
    if(cur + 25 >= scd->point.getNum())
      cur = 0;
    else
      cur += 25;
    break;
  case SoKeyboardEvent::UP_ARROW:
    if(cur + 1 ==scd->point.getNum() )
      cur = 0;
    else
      cur++;
    break;
  case SoKeyboardEvent::PAGE_DOWN:
    if(cur >= 25)
      cur -= 25;
    else
      cur = scd->point.getNum()- 1;
    break;
  case SoKeyboardEvent::DOWN_ARROW :
    if(cur >= 1)
      cur--;
    else
      cur = scd->point.getNum() - 1;
    break;
  
  default:
    break;
  }
  
  if(ev->wasShiftDown()) {
    if(SoKeyboardEvent::isKeyPressEvent(ev,SoKeyboardEvent::LEFT_SHIFT) ||
       SoKeyboardEvent::isKeyPressEvent(ev,SoKeyboardEvent::RIGHT_SHIFT) )
      cw->startSelect(cw);
    cw->label_point(cur);
  }
}

void SoKeySelect::mouseEvent(SoHandleEventAction * node){
  SoMouseButtonEvent * ev = (SoMouseButtonEvent *) node->getEvent();

  if(SoMouseButtonEvent::isButtonReleaseEvent(ev,SoMouseButtonEvent::ANY))
    return;

  if(ev->getButton() == SoMouseButtonEvent::BUTTON1){
 
    const  SoPickedPoint * pt =  node->getPickedPoint();
    if(!pt) return;
    if(!pt->getDetail()) return;
    if(pt->getDetail()->getTypeId() == SoPointDetail::getClassTypeId()){
 
      SoPointDetail * det = (SoPointDetail *) pt->getDetail();

      cur = det->getCoordinateIndex();
 
    }
  }
}

void SoKeySelect::handleEvent(SoHandleEventAction *node){
  SoSeparator::handleEvent(node );
  
  if (node->getEvent()->getTypeId() ==SoKeyboardEvent::getClassTypeId())
    keyEvent((SoKeyboardEvent *) node->getEvent());
  else if( node->getEvent()->getTypeId() == SoMouseButtonEvent::getClassTypeId())
    mouseEvent(node);
  else
    return;
  
  int mIndex = cw->set->materialIndex[cur];
  mat->ambientColor.setValue(cw->mat->diffuseColor[mIndex]);
  tra->translation.setValue(scd->point[cur]);
  cout << "Point: " ;
  int i;
  for(i=0;i<3;i++){
    cout << scd->point[cur][i] << " ";
  }
  cout << "\n";
}
 
