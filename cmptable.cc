/* Copyright 2011 Thomas Flynn */
/*This file is part of manseg.

    manseg is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    manseg is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with manseg.  If not, see <http://www.gnu.org/licenses/>.*/

#include "cmptable.h"

CmpTable::CmpTable(classify * _cl,QWidget * parent)  {
  cl = _cl;

  int n = cl->getNClasses();
  
  qtw = new QTableWidget(n, n);
  int i,j;
  
  for(i=0;i<n;i++){
    for(j=0;j<n;j++){
      QTableWidgetItem * itm = new QTableWidgetItem;
      itm->setFlags(Qt::ItemIsEnabled | Qt::ItemIsUserCheckable);
      itm->setCheckState(Qt::Checked);
      qtw->setItem(i,j,itm);
    }
  }

  connect(qtw,SIGNAL(cellChanged(int,int)),
	  SLOT(cellChanged(int,int)));

  QStringList lbls = getLabels();

  qtw->setHorizontalHeaderLabels(lbls);
  qtw->setVerticalHeaderLabels(lbls);
  
  QVBoxLayout * vlay = new QVBoxLayout;
  QPushButton * qp = new QPushButton(tr("Show all points"));

  connect(qp,SIGNAL(clicked()),SLOT(showAll()));
  vlay->addWidget(qtw);
  vlay->addWidget(qp);
  
 
  
  setLayout(vlay);
  show();


}

void CmpTable::showAll(){

  int npts = aptx->get_npts();

  point * cpt;
 
  int cur_val = 0;
  int i;

  for(i=0;i<npts;i++){
    cpt = aptx->get_pt(i);
    if(cpt->valid == true){
    visIndex->set1Value(cur_val, true);
      cur_val++;
    }
  }

}

void CmpTable::setPtx(labelled_ptx * _aptx, SoMFBool * visI){
  visIndex = visI;
  aptx = _aptx;
}


QStringList CmpTable::getLabels(){

  QStringList qsl;
  int n =cl->getNClasses();
  int i;
  for(i=0;i<n;i++){
    qsl.push_back(cl->getNameByIdx(i).c_str());
  }
  return qsl;
}

void CmpTable::cellChanged(int r,int c){
  printf("pressed row %d col %d\n",r,c);
  QTableWidgetItem * itm = qtw->item(r,c);
  
  if (itm->checkState() == Qt::Checked ){
    printf("showing\n");
    toggle_pts(r,c,true);
  }
  else{
    toggle_pts(r,c,false);
    
  }

}

void CmpTable::toggle_pts(int r, int c, bool show){
  int npts = aptx->get_npts();
  point * cpt;
  lpair cp;
  int cur_val = 0;
  int i;
  for(i=0;i<npts;i++){
    cpt = aptx->get_pt(i);
    if(cpt->valid == true){
      cp = aptx->getBoth(i);
      if (cl->getIdxById(cp.first) == r && cl->getIdxById(cp.second) == c)
	visIndex->set1Value(cur_val, show);
      cur_val++;
    }
  }
  
}
