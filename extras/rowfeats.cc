/* Copyright 2014 Thomas Flynn */
/*This file is part of manseg.

    manseg is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    manseg is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with manseg.  If not, see <http://www.gnu.org/licenses/>.*/

#include "rowfeats.h"
#include "coinwindow.h"

float rowfeats::evals(int r, int c, int rad){
    vector<SbVec3f> pts;
    int id;
    for(int i=-rad;i<=rad;i++){
      for(int j=-rad;j<=rad;j++){
	id = safeid(r+i,c+j);
	if(id != -1){
	  // printf("Pushing back %d\n",id);
	  pts.push_back( cw->pts->point[id] );
	}
      }
    }
    // printf("Got %d valid pts\n",pts.size());
    SbVec3f ctr; Vector3cf eval; Matrix3cf evec;
    planeSelect::get_cov_ctr(pts,ctr,eval,evec);
    Vector3f evals(std::real(eval[0]),std::real(eval[1]),std::real(eval[2]));
    //  printf("ev %f %f %f\n",evals[0], evals[1], evals[2]);
    float mv = evals.minCoeff();
    return mv;
  
  }
 
void plot(vector<float> & vals,bool gplot, int prefix){
  char fname[255];
  sprintf(fname,"%d_temp.txt",prefix);
  FILE * f = fopen(fname,"w");
  if(f == NULL){
    printf("Could not open %s for writing\n",fname);
    return;
  }
  if(gplot) fprintf(f,"plot \"-\" with lines\n# x\n");

  for(int i=0;i<vals.size();i++)
    fprintf(f,"%f\n",vals[i]);

  if (gplot) fprintf(f,"end\npause -1");

  fclose(f);

  if(!gplot) return;

  if(fork() == 0){
    char cmd[255];
    sprintf(cmd,"gnuplot %s",fname);
    system(cmd);
  }
}
