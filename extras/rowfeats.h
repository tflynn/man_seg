/* Copyright 2014 Thomas Flynn */
/*This file is part of manseg.

    manseg is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    manseg is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with manseg.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef __ROWFEATS__
#define __ROWFEATS__

#include <unistd.h>

#include <utility>
#include <vector>
#include <stdio.h>
#include <Inventor/SbLinear.h>

using namespace std;

typedef pair<int,int> ip;
typedef vector<vector<int> > vvi;

class CoinWindow;

class rowfeats{
 public:
  
  //maps point cloud indices to grid locations
  //r: scan line c: within scanline
  void addpt(int r, int c){
    locations.push_back(ip(r,c));
    idxs[r][c] = locations.size();
  }

  void resize(int rows,int cols){
    printf("Doing Resize\n");
    idxs.resize(rows,vector<int>(cols, -1));
    printf("Did resize\n");
  }

  void getid(int &i, int r, int c){
    i = idxs[r][c];
  }

  int safeid(int r,int c){
    if(r < 0 or r >= idxs.size()) return -1;
    if(c < 0 or c >= idxs[r].size()) return -1;
    return idxs[r][c];
  }

  void getloc(int i, int &r, int & c){
    const ip & p = locations[i];
    r = p.first;
    c = p.second;
  }

  float evals(int  r, int c, int rad=1);
  CoinWindow * cw;
  vvi idxs;
  vector<ip> locations;
};

void plot(vector<float> & vals,bool gplot=true, int prefix=0);
#endif
