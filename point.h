/* Copyright 2013 Thomas Flynn */
/*This file is part of manseg.

    manseg is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    manseg is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with manseg.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef __PT__
#define __PT__

#include <math.h>

#define GOOD_POINT(pt) ( ( pt->c[0] == 0.0 && pt->c[1] == 0.0 && pt->c[2] == 0.0 )?false:true )

struct point{
  point() {valid=false; for(int i=0;i<3;i++) norm[i] = c[i] = 0.0; }

  void invalidate(){
    valid=false;
    c[0] = c[1] = c[2] = 0.0;
  }

  #ifdef COIN_SBVEC3F_H
  point(SbVec3f v) {
    valid=true; 
    for(int i=0;i<3;i++){
      norm[i] = 0.0; 
      c[i] = v[i];
    }
  }
  #endif

#ifdef PTX2PLY
  double i;
#endif

  double c[3];
  double norm[3];
  bool valid;
};




#endif
