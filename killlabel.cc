/* Copyright 2013 Thomas Flynn */
/*This file is part of manseg.

    manseg is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    manseg is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with manseg.  If not, see <http://www.gnu.org/licenses/>.*/

#include "ptx.h"
#include "classify.h"
#include <stdio.h>

int main(int argc, char **argv){
 
  if(argc < 7){
    printf("Usage: %s ptx_name orig_lbl_name lbl_definitions lbl_to_remove new_lbl_name new_ptx_name\n",argv[0]);
    return 0;
  }

  labelled_ptx ptc(argv[1],string(argv[2]));
  classify cl(argv[3]);

  int id = cl.getIdByName(argv[4]);
  ptc.kill_label(id);

  ptc.write_labels(argv[5]);
  ptc.write_ptx(argv[6]);
  return 0;
}
