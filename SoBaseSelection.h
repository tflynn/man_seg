/* Copyright 2013 Thomas Flynn */
/*This file is part of manseg.

    manseg is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    manseg is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with manseg.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef __SOBASESELECTION
#define __SOBASESELECTION

#include <Inventor/nodes/SoSubNode.h>
#include <Inventor/nodes/SoSelection.h>
#include <Inventor/nodes/SoExtSelection.h>
#include <Inventor/events/SoEvent.h>
#include <Inventor/events/SoMouseButtonEvent.h> 
#include <Inventor/events/SoLocation2Event.h> 
#include <Inventor/fields/SoSFEnum.h>
#include <Inventor/actions/SoHandleEventAction.h>
#include <Inventor/SbLinear.h>
#include <vector>

using namespace std;

class planeSelect;

class SoBaseSelection : public SoSeparator {

  SO_NODE_HEADER(SoBaseSelection);

  SoGroup * ss;
  SoGroup * cur;

public:

  enum {
    CHOOSING_PLANE,
    CHOOSING_POINTS
  } ;

  int mode();
  planeSelect * ps;
  SbBool validPt(SbVec3f);
  float getDist(SbVec3f);
  void swap();
  SoBaseSelection(SoGroup * _ss = NULL);

  static void initClass();
  static void doneCB(void * bs);
  vector<planeSelect *> pses;
};

#endif
