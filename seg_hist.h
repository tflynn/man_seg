/* Copyright 2011 Thomas Flynn */
/*This file is part of manseg.

    manseg is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    manseg is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with manseg.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef __SEGHIST__
#define __SEGHIST__

#include <utility>
#include <list>

using namespace std;

struct pt_id{
pt_id(int a_id, int a_cur, int a_old):id(a_id),cur(a_cur),old(a_old) {}
  int id;
  int cur;
  int old;
};

typedef struct pt_id pt_id;

class seg: public list<pt_id> {
 public:
 seg(void * p): list<pt_id>(), ptr(p){ }
 seg(): list<pt_id>(){}
  void * ptr;
};

typedef list<seg> seg_list;

class seg_hist{
 public:
  seg undo();
  void new_seg(void * p);
  void add_pt(int a_id, int a_cur, int a_old );
  void push_seg(seg & aseg);
 private:
  seg_list segments;
};

#endif
