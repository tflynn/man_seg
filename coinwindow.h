/* Copyright 2014 Thomas Flynn */
/*This file is part of manseg.

    manseg is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    manseg is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with manseg.  If not, see <http://www.gnu.org/licenses/>.*/

#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoSelection.h>
#include <Inventor/nodes/SoMaterialBinding.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoLineSet.h>
#include <Inventor/nodes/SoIndexedLineSet.h>

#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoPointSet.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/nodes/SoDirectionalLight.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoPerspectiveCamera.h>
#include <Inventor/nodes/SoCone.h>
#include <Inventor/nodes/SoCylinder.h>
#include <Inventor/Qt/SoQt.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include <Inventor/SbViewportRegion.h>
#include <Inventor/nodes/SoPerspectiveCamera.h>
#include <Inventor/details/SoPointDetail.h>
#include <Inventor/manips/SoHandleBoxManip.h>
#include <Inventor/manips/SoTrackballManip.h>
#include <Inventor/actions/SoLineHighlightRenderAction.h>
#include <Inventor/actions/SoBoxHighlightRenderAction.h>
#include <Inventor/actions/SoGetBoundingBoxAction.h>
#include <Inventor/SoPickedPoint.h>
#include <Inventor/actions/SoWriteAction.h>
#include <Inventor/SoOutput.h>
#include <Inventor/nodes/SoExtSelection.h>
#include <Inventor/SoPrimitiveVertex.h>
#include <Inventor/SoDB.h>
#include <Inventor/SbLinear.h>

#include <Inventor/Qt/SoQtCursor.h>
#include <Inventor/fields/SoMFInt32.h>
#include <Inventor/draggers/SoTransformBoxDragger.h>
#include <Inventor/misc/SoChildList.h>

#include <QWidget>
#include <QListWidget>
#include <QListWidgetItem>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QFileDialog>
#include <QRadioButton>
#include <QGroupBox>
#include <QLabel>
#include <QSlider>
#include <QCheckBox>
#include <QDoubleSpinBox>
#include <QCursor>
#include <QtGui/QMouseEvent>
#include <iostream>
#include <utility>
#include <algorithm>

#include "ptx.h"
#include "typelist.h"
#include "classify.h"
#include "seg_hist.h"
#include "planeselect.h"
#include "brushselect.h"
#include "SoPenSelection.h"
#include "SoBaseSelection.h"
#include "SoKeySelect.h"
#include "SoIndexedNew.h"
#include "cmptable.h"
#include "SoCursor.h"
#include "SoQtWorldViewer.h"
#include "extras/rowfeats.h"
using namespace std;

#ifndef COINWINDOW
#define COINWINDOW

typedef vector<int> labelled;


typedef SoQtExaminerViewer hotviewer ;

class CoinWindow : public QWidget
{
  Q_OBJECT
 public:
  CoinWindow(classify * _cl,QWidget * parent = 0);
  
 static SbBool pointCB(void *pData, SoCallbackAction *pAction, 
		     const SoPrimitiveVertex *pVertex);

 static SoPath * featCB(void * pData,const SoPickedPoint * pick);

 SbBool label_point(int coordIndex);

 static void startSelect(void * data, SoSelection *sel = NULL);
 static void undoCallback(void *data, SoEventCallback *node);
 void replaceSelection(SoGroup *);
 SoSeparator * ptx_to_sep(ptx *aptx);

 void setFile(char *file, char * lname= NULL);
 void clearSettingsBox();
 void makeBrushSettings();
 void makePlanarSettings();
 void makeSelectionBox();
 void makeViewWind();
 void redoLastSegment();

 void view_line(char *fname);
 ptx * temp;
 rowfeats rf;
 public slots:
 void brushSlide();
 void planarSlide(double);
 void saveClicked();

 void polyClicked();
 void brushClicked();
 void lassoClicked();
 void rectClicked();
 void featClicked();

 void invertBox(int);
 void keyClicked();
 void boxClicked();
 void compareClicked();
 void elevationClicked();
 void saveOnlyClicked();
 void findEmpty();
 private:

 void initClasses();


 labelled cmp;
 classify * cl;
 TypeList * tl;
 seg_hist * hist;


 QPushButton * saveLabel;
 QWidget * myWindow;
 QSlider * slide;
 
 QDoubleSpinBox * sbox;

 QCheckBox * invert;
 QGroupBox * setbox;
 
 hotviewer * viewer;
 SoCursor * curs;
 SoGroup * pExtSelect; //selection node. contains sep.
 SoSeparator * sep; //under this is the ptx: ptx and set.
 SoCoordinate3 * pts;  //the points
 SoIndexedNew * set; //pointset containing points
 SoMaterial * mat; //colors/labels
 int num_val_pts;

 QCheckBox * planarCheck; 
 SoBaseSelection * planeSel;

 friend class planeSelect;
 friend class brushSelect;
 friend class SoKeySelect;
 friend class rowfeats; //ROWFEAT
 
};

#endif
