/* Copyright 2013 Thomas Flynn */
/*This file is part of manseg.

    manseg is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    manseg is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with manseg.  If not, see <http://www.gnu.org/licenses/>.*/

#include <string.h>
#include <stdio.h>
#include "ptx.h"
#include <algorithm>
#include <string>
using namespace std;

typedef ptx_master ptx;

ptx_master * ptx_master::openptx(char * fname, char *lname){
  int t;
  if(lname != NULL)
    t = PTX_LABEL;
  else
    t = ptx_type(fname);
  
  if(t == PTX_COLOR)
    return new color_ptx(fname);
  else if ( t == PTX_INTEN)
    return new inten_ptx(fname);
   else if ( t == PTX_LABEL){
     //printf("labelled ptx detected\n");
    string f = string(fname);
    string l;
    if(lname == NULL)
      l = "";
    else
      l = string(lname);

    return new labelled_ptx(f,l);
  }
}  

ptx_master::~ptx_master(){
  
  if(!ascii) 
    fclose(f);
  else
    for(int i=0;i<rows*cols;i++)
      delete pts[i];
}

void ptx::cart_to_sphere(point *pt, double *r, double *t, double *p){
  double r1;
  r1= sqrt(pow(pt->c[0], 2) + pow(pt->c[1], 2) + pow(pt->c[2], 2));
  if(r) *r = r1;
  
  if(r1 < 0.01){ //empty point
    if(t) *t = 0;
    if(p) *p = 0;
    return;
  }

   //switched version:
  /*  if(t) *t=atan2(pt->c[2],pt->c[0]); //theta
      if(p) *p=asin(pt->c[1]/r1); //phi*/

  //unswitched version:
    if(t) *t=atan2(-pt->c[1],pt->c[0]); //theta
 
   if(p) *p=asin(pt->c[2]/r1); //phi
}

bool ptx::read_ptx(const string &fname){
  return read_ptx(fname.c_str()) ;
}


ptx_master::ptx_master(){
  rows = cols = 0;
  start = 0;
  f = 0;
}

ptx_master::ptx_master(const ptx & cpyptx){
  ascii = cpyptx.ascii;

  if(!cpyptx.ascii)
    printf("Copying binary ptx...This ptx will close when the other does!\n");
  
  
  int i,tot;

  // intensities = cpyptx.intensities;
  file_name = cpyptx.file_name;
  rows = cpyptx.rows;
  cols = cpyptx.cols;

  tot=rows*cols;

  start = cpyptx.start;
  f = cpyptx.f;

  //if the file is in binary mode,
  //we get the current pos, read whole ptx and then set back.

  //  int cpoint; //current filemarker
  if(ascii){
    pts.resize(rows*cols);
    for(i=0;i<tot;i++)
      pts[i] = new point(*cpyptx[i]);
  }

}

void ptx::get_params(double &minp, double &dp, double &mint, double &dt){
  //first go through all scan lines and use neighbors to estimate dp.
  int i,j;
  
  double prev;
  double cur;
  double sum;

  double minest;

  int cnt;
  point *cpt;
  point *ppt;

  sum = 0.0;
  cnt = 0;
  minest = 0.0;
  //first get dp.

  for(i=0;i<rows;i++){
    ppt = NULL;
    for(j=0;j<cols;j++){
      cpt = get_pt(i,j);
      if(ppt){
	if(cpt->valid){
	  cart_to_sphere(cpt,NULL,NULL,&cur);
	  sum += fabs(cur - prev);
	  cnt++;
	  minest += cur - fabs((cur-prev))*j;
	}
      }
      if(cpt->valid) {
	prev = cur;
	ppt = cpt;
      }
      else{
	ppt = NULL;
      }
    }
  }
  
  dp = sum/cnt;
  minp = minest/cnt;

  ppt = NULL;
  sum = 0.0;
  cnt = 0;
  minest = 0.0;
  cur=0.0;

  //now dt
  for(i=0;i<cols;i++){
    ppt = NULL;
    for(j=0;j<rows;j++){
      cpt = get_pt(j,i);
      if(ppt){
	if(cpt->valid){
	  cart_to_sphere(cpt,NULL,&cur,NULL);
	  sum += fabs(cur - prev);
	  cnt++;
	  minest += cur - fabs((cur - prev))*j;
	}
      }
      if(cpt->valid){
	prev = cur;
	ppt = cpt;
      }
      else{
	ppt = NULL;
      }
    }
  }
  
  dt = sum/cnt;
  mint = minest/cnt;
}

ptx_master::ptx_master(string fname, bool ascii_ ){
  if(ascii_)
    read_ptx(fname.c_str());
  else
    open_ptx_bin(fname.c_str());
}

void ptx::get_color(int , unsigned char &, unsigned char &, unsigned char &){
  /* double inten = intensities[i];
  r = (int)(254*inten);
  g = (int)(254*inten);
  b = (int)(254*inten);*/
  }

void ptx::get_color(int i, int j,unsigned  char &r,unsigned  char &g, unsigned char &b){
 get_color(i*cols+j,r,g,b);
}


void ptx::get_nbd(int row, int col, int &sz, point **res, int NBD_RAD){
  int size;
  size=(2*NBD_RAD+1) * (2*NBD_RAD+1);
  
  int i,j;
  point *pt;
  int pos=0;

  for(i=0;i<size;i++)
    res[i]=NULL;

  pt = get_pt(row*cols + col);
  
  res[pos++] = new point(*pt);
 
  for(i=-1*NBD_RAD;i<=NBD_RAD;i++){
      for(j=-1*NBD_RAD;j<=NBD_RAD;j++){
	if(i==0 && j==0) continue;
	if (row + i >= 0 && row + i < rows
	    && col + j >= 0 && col + j < cols){
	  pt = get_pt((row + i)*cols + col + j);
	  if(pt->valid)
	    res[pos++] = new point(*pt);
	}
      }
  }

  sz = pos;
}

int ptx::get_npts(const string &fname){
  return get_npts(fname.c_str());
}

int ptx::get_npts(const char *fname){
   FILE *f_pts;

  int i,j;

  if (!(f_pts = fopen(fname,"r"))){
    fprintf(stderr,"Could not open %s for reading\n",fname);
    return 0;
  }
  fscanf(f_pts,"%d",&i);
  fscanf(f_pts,"%d",&j);
  fclose(f_pts);
  return i*j;

}

bool ptx::read_ptx(const char *fname){
  ascii = true;
  file_name = string(fname);
  FILE *f_pts;
  double dummy[4];
  int i;
  //  printf("reading non color ptx\n");
  if (!(f_pts = fopen(fname,"r"))){
    fprintf(stderr,"Could not open %s for reading\n",fname);
    return false;
  }
  //printf("Opening a file in ascii!\n");
  fscanf(f_pts,"%d",&rows);
  fscanf(f_pts,"%d",&cols);

  for (int i=0; i<4; ++i)
    fscanf(f_pts, "%lf %lf %lf\n", &dummy[0], &dummy[1], &dummy[2]);
   
  for (int i=0; i<4; ++i)
    fscanf(f_pts, "%lf %lf %lf %lf\n", &dummy[0], &dummy[1], &dummy[2], &dummy[3]);
  
  point *pt;
  
  int total = rows*cols;
  for(i=0;i<total;i++){
    pt = new point;
    fscanf(f_pts,"%lf %lf %lf %lf", &pt->c[0],&pt->c[1],&pt->c[2],&dummy[0]);
    
#ifdef PTX2PLY
    pt->i = dummy[0];
#endif

    pt->valid=GOOD_POINT(pt);
    pts.push_back(pt);
  }
  fclose(f_pts);
  return true;
}

int ptx::write_ptx(const string &fname){
  return write_ptx(fname.c_str());
}

int ptx::write_ptx(const char *){
  return -1;
}

template <class X> void ptx::each_point(void (*pt_fn)(point *, X), X u){
    for(int i=0;i<rows*cols;i++)
      (*pt_fn)( get_pt(i), u);
}

template <class X> void ptx::each_point(void (*pt_fn)(point *, int, int, X), X u){
    int i,j;
    for(i=0;i<rows;i++)
      for(j=0;j<cols;j++)
        (*pt_fn)( get_pt(i*cols + j),i,j, u);
}

void ptx::open_ptx_bin(const char *fname){
  ascii = false;
  //  printf("Opening %s in binary format\n",fname);
  file_name = string(fname);
  assert(f = fopen(fname,"r"));
  
  fread(&rows,sizeof(int),1,f);
  fread(&cols,sizeof(int),1,f);
  //printf("Rows: %d, ",rows);
  //printf("Cols: %d.\n",cols);
  start = ftell(f);
}

void ptx::read_ptx_bin(){
  int total = rows*cols;
  int i;
  point *tmp;
  fseek(f,start,SEEK_SET);
  pts.clear();
  for(i=0;i<total;i++){
    tmp = new point;
    fread(tmp, sizeof(point), 1,f);
    pts.push_back(tmp);
  }
  fclose(f);
  ascii = true;
}

void ptx::write_ptx_bin(const string &fname){
  write_ptx_bin(fname.c_str());
}

void ptx::write_ptx_bin(const char *fname){
  FILE * f_out;
  f_out = fopen(fname,"w");
  int i;

  fwrite(&rows,sizeof(int),1,f_out);
  fwrite(&cols,sizeof(int),1,f_out);

  //  fprintf(f_out,"%d\n%d\n",rows,cols);
  for(i=0;i<rows*cols;i++)
    fwrite(get_pt(i),sizeof(point),1,f_out);
  fclose(f_out);
}


point * ptx::get_pt(int i){
  if(i >= rows*cols) return NULL;
  if(ascii) return pts[i];
  point *ret = new point;
  fseek(f,start + sizeof(point)*i,SEEK_SET);
  fread(ret,sizeof(point),1,f);
  return ret;
}

void ptx::get_pt(int r, int c, double *& pt){
  int i = (cols*r) + c; 
  if( i > rows*cols){
    pt =  NULL;
  }
  else if(ascii){
 
    if(pts[i]->valid){
      pt = pts[i]->c;
    }
    else 
      pt = NULL;
  }
  else{
    point * ret = new point;
    fseek(f,start + sizeof(point)*i,SEEK_SET);
    fread(ret,sizeof(point),1,f);
    
    if(ret->valid)
      pt = ret->c;
    else
      pt = NULL;
  }

}

point * ptx::get_pt(int r, int c){
  return get_pt( (cols * r) + c );
}

const point * ptx::get_pt(int i) const{
  if(i >= rows*cols) return NULL;
  if(ascii) return pts[i];
  point *ret = new point;
  fseek(f,start + sizeof(point)*i,SEEK_SET);
  fread(ret,sizeof(point),1,f);
  return ret;
}

const point * ptx::operator[](int i) const{
  return get_pt(i);
}

point * ptx::operator[](int i){
  return get_pt(i);
}

point * ptx::operator()(int r, int c){
  return get_pt( (cols*r) + c);
}

template<class T>
point * ptx_collection<T>::get_pt(int n){
  //search for ptx containing i'th point
  int sz = ptxs.size();
  int i;
  int cur=0;
  int total=0;
  for(i=0;i<sz;i++){
    cur = ptxs[i]->get_npts();
    if(total <= n && n < total + cur)
      return ptxs[i]->get_pt(n-total);
    else
      total+=cur;
  }
  return NULL;
}

template<class T>
point * ptx_collection<T>::get_pt(int p, int r, int c){
  ensure_presence(p);

  ptx * theptx = ptxs[p];

  int pt;
  pt = (r * theptx->get_cols()) + c;
  return theptx->get_pt(pt);
}

template<class T>
point * ptx_collection<T>::operator()(int p, int r, int c){
  return get_pt(p,r,c);
}


template<class T>
ptx_collection<T>::~ptx_collection(){
  delete_all();
  ptxs.clear();
}
  
template<class T>
void ptx_collection<T>::delete_all(){
  int i,sz;
  sz = ptxs.size();
  for(i=0;i<sz;i++){
    if(ptxs[i]) delete ptxs[i];
    ptxs[i] = NULL;
  }
}

template <class T>
template <class X> 
void ptx_collection<T>::each_point(void (*pt_fn)(point *, X), X u){
    int sz = ptxs.size();
    for(int i=0;i<sz;i++)
      ptxs[i]->each_point<X>(pt_fn, u);
  }

template<class T>
void ptx_collection<T>::ensure_presence(int i){
  
  if(oneptx && !ptxs[i]){
    delete_all();
    
    int t = ptx_type(fnames[i].c_str());
    ptx *tmp;
    if(t == 2){
      //printf("gonna make a new clr ptx\n");
      tmp = new color_ptx(fnames[i].c_str());
    }
    else{
      //printf("gonna make a new inten ptx\n");
      tmp = new inten_ptx(fnames[i].c_str());
    }
    ptxs[i] =tmp;
  }

}

template<class T>
ptx_collection<T>::ptx_collection(const char * dir, bool ascii){
  open(string(dir),ascii);
}

template<class T>
ptx_collection<T>::ptx_collection(){

  total_points = 0;
  thedir = "";
  oneptx=false;
}

template<class T>
ptx_collection<T>::ptx_collection(const string dir, bool ascii){ 
  oneptx=false;
  //printf("in string,bool constructor of ptx_collection\n");
  open(dir,ascii);
  
}

template<class T>
int ptx_collection<T>::get_rows(int i){
  ensure_presence(i);
  return ptxs[i]->get_rows();
}

template<class T>
int ptx_collection<T>::get_cols(int i){ 
  ensure_presence(i);
  return ptxs[i]->get_cols();
}

template<class T>
void ptx_collection<T>::set_dir(string newdir){
  thedir=newdir;
  mkdir(thedir.c_str(),0777);
}

template<class T>
void ptx_collection<T>::toggle_oneptx(bool newval){
  oneptx =newval;
  if(oneptx)
    delete_all();
}

template<class T>
void ptx_collection<T>::write_all(){
  int i;
 
  if(thedir[thedir.size()-1] != '/') thedir+="/";
  string cname;
  for(i=0;i<ptxs.size();i++){
    cname = thedir + itoa(i) + ".ptx";
    //printf("writing %s\n",cname.c_str());
    //cout << "writing: " << cname << endl;
    ptxs[i]->write_ptx(cname.c_str());
  }


}

template<class T>
void ptx_collection<T>::open(const string dir, bool ascii){
  //printf("in open\n");
  thedir=dir; //thedir is just the directory where files will be written if write_all is called.

  if(thedir[thedir.size()-1] != '/') thedir += "/";

  total_points = 0;
 
  add_dir(dir,ascii);

}

template<class T>
void ptx_collection<T>::add_dir(const string mydir, bool ascii){
  vector<string> cfnames;
  string dir(mydir);
  if(dir[dir.size()-1] != '/') dir += "/";
    
  getdir(dir,cfnames);
  int i;

  if(cfnames.size() == 0){
    //cout << "no ptx files in " << dir << endl;
    //printf("no ptx files in %s\n",dir.c_str());
    return;
  }
  // printf("in add_dir\n");
  for(i=0;i<cfnames.size();i++)
    add_ptx(cfnames[i],ascii);

}

template<class T>
T * ptx_collection<T>::get_ptx(int i){ 

  if(i < ptxs.size()){
    return ptxs[i]; 
  }
  else{
    //printf("no ptx # %d. only %d ptxs loaded\n",i,ptxs.size());
    return NULL;
  }

}

template<class T>
int ptx_collection<T>::get_npts(){
  return total_points;
}


template<class T>
void  ptx_collection<T>::add_ptx(const string &fname, bool ascii){
  printf("about to add a ptx\n");

  int t = ptx_type(fname.c_str());
 
  ptx *tmp;

  if(t == 2)
    tmp = new color_ptx(fname,ascii);
  else
    tmp = new inten_ptx(fname,ascii);
  
  if(!oneptx){
    //tmp = new T(fname,ascii);
    total_points+=tmp->get_npts();
  }
  else{
    tmp = NULL;
    total_points+=ptx::get_npts(fname);
  }
  
  fnames.push_back(fname);
  ptxs.push_back(tmp);

}

template<class T>
point * ptx_collection<T>::random_pt(int start){
  int t = total_points - start;
  int r = start + rand()%t;
  
  if(r==start) return get_pt(r);

  point *tmp = get_pt(r); //pointer to point r
  point *cpt = get_pt(start); //point to point at start.
  
  point cp(*cpt); //copy of point at start.
  
  cpt->valid=tmp->valid;
  int i;
  for(i=0;i<3;i++){
    cpt->c[i] = tmp->c[i];
    cpt->norm[i] = tmp->norm[i];
  }

  //now change point r to point cp.

  tmp->valid = cp.valid;
  for(i=0;i<3;i++){
    tmp->c[i] = cp.c[i];
    tmp->norm[i] = cp.norm[i];
  }

  return cpt;
}

template<class T>
void ptx_collection<T>::random_unique_pts(vector<pair<point *, point *> > & pts,
				       int npts){
  int i;
  for(i=pts.size()-1;i>=0;delete pts[i].first, delete pts[i].second, i--);
  pts.clear();
 
  point *tmp;
  pts.resize(npts);
  for(i=0;i<npts;i++){
    tmp = random_pt(i);
    //cout << tmp->c[0] << "," << tmp->c[1] << "," << tmp->c[2] << endl;
    pts[i] = pair<point *, point *>(new point(*tmp), NULL);
  }

}

template<class T>
void ptx_collection<T>::operator+=(const ptx_collection * more_ptxs){

 
  total_points+=more_ptxs->total_points;

  int i;
  for(i=0;i<more_ptxs->ptxs.size();i++)
    ptxs.push_back(more_ptxs->ptxs[i]);

  for(i=0;i<more_ptxs->fnames.size();i++)
    fnames.push_back(more_ptxs->fnames[i]);

}


/*adapted from http://www.linuxquestions.org/questions/programming-9/c-list-files-in-directory-379323/ */
/*function... might want it in some class?*/
/*gets all files in a dir*/

template<class T>
 void ptx_collection<T>::getdir (const string dir, vector<string> &files)
{
    DIR *dp;
    struct dirent *dirp;
    string dname;

    if(!(dp = opendir(dir.c_str()))){
      printf("Couldn't open directory %s\n",dir.c_str());
      return;
    }

    while ((dirp = readdir(dp)) != NULL) {
      dname = string(dirp->d_name);
      if(dirp->d_type == DT_DIR) continue;
      if(dname != "." && dname != ".." ) files.push_back(dir + string(dname));
    }
    closedir(dp); 
}

bool color_ptx::read_ptx(const char *fname){
  printf("reading a color ptx\n");
  ascii = true;
  file_name = string(fname);
  FILE *f_pts;
  double dummy[4];
  int i;

  if (!(f_pts = fopen(fname,"r"))){
    fprintf(stderr,"Could not open %s for reading\n",fname);
    return false;
  }

  fscanf(f_pts,"%d",&rows);
  fscanf(f_pts,"%d",&cols);

  for (int i=0; i<4; ++i)
    fscanf(f_pts, "%lf %lf %lf\n", &dummy[0], &dummy[1], &dummy[2]);

  for (int i=0; i<4; ++i)
    fscanf(f_pts, "%lf %lf %lf %lf\n", &dummy[0], &dummy[1], &dummy[2], &dummy[3]);

  point *pt;

  int total = rows*cols;
  char *clr_c;
  colors.resize(total);
  for(i=0;i<total;i++){
    pt = new point;
    clr_c= new char[3];
    fscanf(f_pts,"%lf %lf %lf %lf %lf %lf %lf\n", &pt->c[0],&pt->c[1],&pt->c[2],
	   &dummy[0], &dummy[1],&dummy[2],&dummy[3]);

#ifdef PTX2PLY
    pt->i = (1./3.)*(double)(clr_c[0]+clr_c[2]+clr_c[3]);
#endif
    clr_c[0] = (char)dummy[1];
    clr_c[1] = (char)dummy[2];
    clr_c[2] = (char)dummy[3];

    colors[i] = clr_c;
    pt->valid=GOOD_POINT(pt);
    pts.push_back(pt);
  }
  fclose(f_pts);
  return true;
}


/**
 * C++ version 0.4 std::string style "itoa":
 * Contributions from Stuart Lowe, Ray-Yuan Sheu,
 * Rodrigo de Salvo Braz, Luc Gallant, John Maloney
 * and Brian Hunt
 */
/*from http://www.jb.man.ac.uk/~slowe/cpp/itoa.html*/
template<class T>
string ptx_collection<T>::itoa(int value, int base) {
  
  std::string buf;
  
  // check that the base if valid
  if (base < 2 || base > 16) return buf;
  
  enum { kMaxDigits = 35 };
  buf.reserve( kMaxDigits ); // Pre-allocate enough space.
  
  int quotient = value;
  
  // Translating number to string with base:
  do {
    buf += "0123456789abcdef"[  quotient % base ];
    quotient /= base;
  } while ( quotient );
  
  // Append the negative sign
  if ( value < 0) buf += '-';
  
  std::reverse( buf.begin(), buf.end() );
  return buf;
}


template class ptx_collection<ptx>;
//template class ptx_collection<color_ptx>;

void color_ptx::get_color(int i, unsigned char &r, unsigned char &g, unsigned char &b){
  r = colors[i][0];
  g = colors[i][1];
  b = colors[i][2];
}

void color_ptx::get_color(int i,int j, unsigned char &r, unsigned char &g, unsigned char &b){
  get_color(i*cols+j,r,g,b);
}

int color_ptx::write_ptx(const char *fname){
  printf("in color write\n");
  int nrows, ncols;
  nrows= rows; ncols =cols;
  FILE *f_out;
  int i;
  int j;
  
  if(!(f_out=fopen(fname,"w"))){
    printf("unable to open %s for writing.\n",fname);
    return 1;
  }
  
  fprintf(f_out,"%d\n",nrows);
  fprintf(f_out,"%d\n",ncols);
  fprintf(f_out,"0.000000 0.000000 0.000000\n1.000000 0.000000 0.000000\n0.000000 1.000000 0.000000\n0.000000 0.000000 1.000000\n1.000000 0.000000 0.000000 0.000000\n0.000000 1.000000 0.000000 0.000000\n0.000000 0.000000 1.000000 0.000000\n0.000000 0.000000 0.000000 1.000000\n");
  point *pt;
  float r,g,b;

  int cpt;
  for(i=0;i<nrows;i++){
    for(j=0;j<ncols;j++){
      cpt = i*ncols + j;
      pt = get_pt(cpt);

      if(colors.size() > cpt){
	r = (float)colors[cpt][0];
        g = (float)colors[cpt][1];
        b = (float)colors[cpt][2];
      }
     
      fprintf(f_out,"%lf %lf %lf 0.5 %lf %lf %lf\n",pt->c[0],pt->c[1],pt->c[2],r,g,b);
    }
  }
  fclose(f_out);
  return 0;
}


bool inten_ptx::read_ptx(const char *fname){
  //printf("in cool inten rdptx\n");
  ascii = true;
  file_name = string(fname);
  FILE *f_pts;
  double dummy[4];
  int i;
  //printf("reading non color ptx\n");
  if (!(f_pts = fopen(fname,"r"))){
    fprintf(stderr,"Could not open %s for reading\n",fname);
    return false;
  }
  //printf("Opening a file in ascii!\n");
  fscanf(f_pts,"%d",&rows);
  fscanf(f_pts,"%d",&cols);

  for (int i=0; i<4; ++i)
    fscanf(f_pts, "%lf %lf %lf\n", &dummy[0], &dummy[1], &dummy[2]);
   
  for (int i=0; i<4; ++i)
    fscanf(f_pts, "%lf %lf %lf %lf\n", &dummy[0], &dummy[1], &dummy[2], &dummy[3]);
  
  point *pt;
  
  int total = rows*cols;
  for(i=0;i<total;i++){
    pt = new point;
    fscanf(f_pts,"%lf %lf %lf %lf", &pt->c[0],&pt->c[1],&pt->c[2],&dummy[0]);
    
#ifdef PTX2PLY
    pt->i = dummy[0];
#endif
    inten.push_back((float)dummy[0]);
    pt->valid=GOOD_POINT(pt);
    pts.push_back(pt);
  }
  fclose(f_pts);
  return true;
}

void inten_ptx::get_color(int i, unsigned char &r, unsigned char &g, unsigned char &b){
  float c = inten[i];

  r= (char)(c*254.);
  g= (char)(c*254.);
  b= (char)(c*254.);
}

void inten_ptx::get_color(int i,int j, unsigned char &r, unsigned char &g, unsigned char &b){
  get_color(i*cols+j,r,g,b);
}


int inten_ptx::write_ptx(const char *fname){
  printf("in inten write\n");
  int nrows, ncols;
  nrows= rows; ncols =cols;
  FILE *f_out;
  int i;
  int j;
  
  if(!(f_out=fopen(fname,"w"))){
    printf("unable to open %s for writing.\n",fname);
    return 1;
  }
  
  fprintf(f_out,"%d\n",nrows);
  fprintf(f_out,"%d\n",ncols);
  fprintf(f_out,"0.000000 0.000000 0.000000\n1.000000 0.000000 0.000000\n0.000000 1.000000 0.000000\n0.000000 0.000000 1.000000\n1.000000 0.000000 0.000000 0.000000\n0.000000 1.000000 0.000000 0.000000\n0.000000 0.000000 1.000000 0.000000\n0.000000 0.000000 0.000000 1.000000\n");
  point *pt;
  double intensity;
  int cpt;
  for(i=0;i<nrows;i++){
    for(j=0;j<ncols;j++){
      cpt = i*ncols + j;
      pt = get_pt(cpt);

      if(inten.size() > cpt)
	intensity = inten[cpt];
      else
	intensity = 0.5;

      fprintf(f_out,"%lf %lf %lf %lf\n",pt->c[0],pt->c[1],pt->c[2],intensity);
    }
  }
  fclose(f_out);
  return 0;
}

int ptx_type(const char *fname){
  FILE *f_pts;
  double dummy[7];
 
  if (!(f_pts = fopen(fname,"r"))){
    fprintf(stderr,"Could not open %s for reading\n",fname);
    return -1;
  }
  printf("fname: %s\n",fname);
  int rows, cols;
  fscanf(f_pts,"%d",&rows);
  fscanf(f_pts,"%d",&cols);
  
  for (int i=0; i<4; ++i)
    fscanf(f_pts, "%lf %lf %lf\n", &dummy[0], &dummy[1], &dummy[2]);
   
  for (int i=0; i<4; ++i)
    fscanf(f_pts, "%lf %lf %lf %lf\n", &dummy[0], &dummy[1], &dummy[2], &dummy[3]);
  
  char * lptr = NULL;
  size_t len = 0;
  getline(&lptr, &len, f_pts);
  
  char *pch = strtok(lptr," ");
  int cnt=0;

  while(pch != NULL){
    cnt++;
    pch = strtok(NULL, " ");
  }
  //printf("cnt: %d\n",cnt);
  
  
  //  int total;
  //total = fscanf(f_pts,"%lf %lf %lf %lf %lf %lf %lf\n", &dummy[4], &dummy[5], &dummy[6], &dummy[0], &dummy[1], &dummy[2], &dummy[3]);
  fclose(f_pts);

  string lbl_name(fname);
  lbl_name = lbl_name + ".lbl";
  bool label_exists = file_exists(lbl_name);

  if(cnt >= 7)
    return PTX_COLOR;
  else{
    if(label_exists) return PTX_LABEL;
    return PTX_INTEN;
  }
}

void labelled_ptx::compare(const char * fname){
  double dummy[4];
  int i;
  int total = rows*cols;
  FILE * f_lbl;
  
  printf("loading label file %s",fname);

  if(!(f_lbl = fopen(fname,"r"))){
    printf("could not open label file %s\n",fname);
  }
      
  int nrgns;
  fscanf(f_lbl,"%s\n",&nrgns);
 
  int lbl,  dum_lbl;

  for(i=0;i<total;i++){

    fscanf(f_lbl,"%d %d",&lbl,&dum_lbl);
    
    cmplabel.push_back(lbl);
 
  }

  fclose(f_lbl);

}

void labelled_ptx::write_labels(const string & lbl_out){

  FILE * f_lbl = fopen(lbl_out.c_str(),"w");
  int npts = get_npts();
  
  fprintf(f_lbl,"0\n");

  int cur_val=0;
  point *cpt;
  int lbl;
  int i;

  for(i=0;i<npts;i++){
    cpt = get_pt(i);
    if(cpt->valid == true){
      lbl = label[i];
    }
    else{
      lbl = -20;
    }
    
    fprintf(f_lbl,"%d 0\n",lbl);
  }

  fclose(f_lbl);
  
}

bool labelled_ptx::read_ptx(const char *fname){
  //printf("reading ptx of labelled\n");
  ascii = true;
  file_name = string(fname);
  FILE *f_pts;
  double dummy[4];
  int i;
  if (!(f_pts = fopen(fname,"r"))){
    fprintf(stderr,"Could not open %s for reading\n",fname);
    return false;
  }
  fscanf(f_pts,"%d",&rows);
  fscanf(f_pts,"%d",&cols);

  for (int i=0; i<4; ++i)
    fscanf(f_pts, "%lf %lf %lf\n", &dummy[0], &dummy[1], &dummy[2]);
   
  for (int i=0; i<4; ++i)
    fscanf(f_pts, "%lf %lf %lf %lf\n", &dummy[0], &dummy[1], &dummy[2], &dummy[3]);
  
  point *pt;
  
  int total = rows*cols;
  
  FILE * f_lbl;
  
  string lbl_name;
  if(label_name == ""){
    lbl_name = fname;
    lbl_name = lbl_name + ".lbl";
  }
  else{
    lbl_name = label_name;
  }

  printf("loading label file %s",lbl_name.c_str());

  if(!(f_lbl = fopen(lbl_name.c_str(),"r"))){
    printf("could not open label file %s\n",lbl_name.c_str());
  }
  
    
  int nrgns;
  fscanf(f_lbl,"%s\n",&nrgns);
  
  int lbl,  dum_lbl;
  printf("reading %d points\n",total);
  for(i=0;i<total;i++){
    pt = new point;
    fscanf(f_pts,"%lf %lf %lf %lf", &pt->c[0],&pt->c[1],&pt->c[2],&dummy[0]);
    fscanf(f_lbl,"%d %d",&lbl,&dum_lbl);
    
    label.push_back(lbl);
    inten.push_back((float)dummy[0]);
    pt->valid=GOOD_POINT(pt);
    pts.push_back(pt);
  }
  fclose(f_pts);
  return true;
}

void labelled_ptx::get_label(int i, char &l){
  if(i >= rows*cols) return;
  l = label[i];
}

void labelled_ptx::get_label(int r, int c, char &l){
  get_label( (cols * r) + c , l);
}

pair<char, char> labelled_ptx::getBoth(int i){
  return pair<char, char>(label[i],cmplabel[i]);
}

void labelled_ptx::kill_label(int lbl){
  int npts = rows*cols;
  char l;
  int i;
  for(i=0;i<npts;i++){
    get_label(i,l);
    if(l == lbl)
      get_pt(i)->invalidate();
  }
}

bool file_exists(string strFilename) {
  struct stat stFileInfo;
  bool blnReturn;
  int intStat;

  // Attempt to get the file attributes                                                                            
  intStat = stat(strFilename.c_str(),&stFileInfo);
  if(intStat == 0) {
    // We were able to get the file attributes                                                                     
    // so the file obviously exists.                                                                               
    blnReturn = true;
  } else {
    // We were not able to get the file attributes.                                                                
    // This may mean that we don't have permission to                                                              
    // access the folder which contains this file. If you                                                          
    // need to do that level of checking, lookup the                                                               
    // return values of stat which will give you                                                                   
    // more details on why stat failed.                                                                            
    blnReturn = false;
  }

  return(blnReturn);
}
