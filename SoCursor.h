/* Copyright 2013 Thomas Flynn */
/*This file is part of manseg.

    manseg is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    manseg is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with manseg.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef _SOCURSOR_
#define _SOCURSOR_
#include <Inventor/nodes/SoSubNode.h>
#include <Inventor/nodes/SoNode.h>
#include <Inventor/events/SoLocation2Event.h>
#include <Inventor/actions/SoHandleEventAction.h>
#include <Inventor/SbViewportRegion.h>
#include <Inventor/actions/SoGLRenderAction.h>
#include <GL/gl.h>
#include <GL/freeglut.h>
#include <Inventor/fields/SoSFVec2s.h>
#include <Inventor/fields/SoSFFloat.h>

#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>

class SoCursor : public SoNode{

  SO_NODE_HEADER(SoCursor);

 public:
  SoSFVec2s pos;
  SoSFFloat rad;

  SoCursor(){
    SO_NODE_CONSTRUCTOR(SoCursor);
    SO_NODE_ADD_FIELD(pos, (0,0));
    SO_NODE_ADD_FIELD(rad, (0));
  }
  
  static void initClass(){
    SO_NODE_INIT_CLASS(SoCursor,SoNode,"Node");
  }


  virtual void GLRender(SoGLRenderAction *action){
    if(rad.isConnected() == false)
      return;

    GLint viewport[4];
    glGetIntegerv(GL_VIEWPORT,viewport);

    GLdouble mvmatrix[16];
    GLdouble projmatrix[16];
    GLdouble wx,wy,wz;

    glPushMatrix();
    glLoadIdentity();
    glColor3f(0,0,0);
    glGetDoublev(GL_MODELVIEW_MATRIX,mvmatrix);
    glGetDoublev(GL_PROJECTION_MATRIX,projmatrix);
    SbVec2s spos = pos.getValue();
    gluUnProject( (GLdouble)spos[0], (GLdouble)spos[1], 0.1,
		  mvmatrix, projmatrix, viewport,
		  &wx, &wy, &wz);
   
    float div = viewport[2] < viewport[3] ? viewport[2] : viewport[3];

    //  printf("%d %d %d %d\n",viewport[0],viewport[1],viewport[2],viewport[3]);
    float r =rad.getValue()*0.1/div;
    // printf("%f\n",r);
  
    glTranslatef(wx,wy,wz); 
 
    int num_segments = 100;
    float theta = 2 * 3.1415926 / float(num_segments); 
    float c = cosf(theta);//precalculate the sine and cosine
    float s = sinf(theta);
    float t;
  
    float x = r;//we start at angle = 0 
    float y = 0; 
    
    glBegin(GL_LINE_LOOP); 
    for(int ii = 0; ii < num_segments; ii++) 
      { 
	glVertex3f(x, y,0);//output vertex 
        
	//apply the rotation matrix
	t = x;
	x = c * x - s * y;
	y = s * t + c * y;
      } 
 
   
    glEnd();        
    

   glPopMatrix();
 
  }
  
  void mouseEvent(SoHandleEventAction * node){
    pos.setValue( node->getEvent()->getPosition() );
    /*    if(pos.isIgnored())
      printf("pos ignored\n");
    
      printf("updated mouse pos\n");*/
  }

  virtual void handleEvent(SoHandleEventAction * node){
     SoNode::handleEvent(node);
     if(node->getEvent()->getTypeId() == SoLocation2Event::getClassTypeId())
      mouseEvent(node);   
  }

};

#endif
