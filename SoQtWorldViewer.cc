/* Copyright 2013 Thomas Flynn */
/*This file is part of manseg.

    manseg is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    manseg is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with manseg.  If not, see <http://www.gnu.org/licenses/>.*/

#include "SoQtWorldViewer.h"

SbBool SoQtWorldViewer::processSoEvent(const SoEvent * const ev){

    SoType type(ev->getTypeId());
    SoCamera * cam = getCamera();
    bool move_trans= false;
    bool move_rot = true;
  
    SbVec3f dpos;

    if(ignore)
      return SbBool(ignore = false);
    printf("PROCESS\n");
    if (type.isDerivedFrom(SoKeyboardEvent::getClassTypeId())){
      SoKeyboardEvent * event = (SoKeyboardEvent *)ev;
      printf("proccess keyevent\n");
      switch(event->getKey()){
      case SoKeyboardEvent::W:
	move_trans=true;
	dpos.setValue(0,0,-1);
	break;
      case SoKeyboardEvent::S:
	move_trans=true;
	dpos.setValue(0,0,1);
	break;
      case SoKeyboardEvent::A:
	move_trans=true;
	dpos.setValue(-1,0,0);
	break;
      case SoKeyboardEvent::D:
	move_trans=true;
	dpos.setValue(1,0,0);
	break;
      case SoKeyboardEvent::R:
	move_trans=true;
	dpos.setValue(0,1,0);
	break;
      case SoKeyboardEvent::F:
	move_trans=true;
	dpos.setValue(0,-1,0);
	break;
      case SoKeyboardEvent::Q:
	if(SoKeyboardEvent::isKeyPressEvent(ev,SoKeyboardEvent::Q))
	  if(!isViewing()){
	    //printf("Viewing was disabled. now enabling\n");
	    setViewing(SbBool(true));
	    setComponentCursor( SoQtCursor::getBlankCursor());
	  }
	  else{
	    printf("enabling cursor\n");
	    setCursorEnabled(SbBool(true));
	    setComponentCursor( SoQtCursor() );
	    setViewing(SbBool(false));
	  }
      
	break;
      deafault:
	break;
      }
      if(move_trans){
	SbVec3f dpos_r,newpos;
	cam->orientation.getValue().multVec(dpos,dpos_r);
	newpos = cam->position.getValue() + dpos_r;
	cam->position.setValue(newpos);
      }
      
    }
    
    if(isViewing() ){
   
      if(SoMouseButtonEvent::isButtonPressEvent(ev, SoMouseButtonEvent::BUTTON2) ){
	printf("process button\n");
	SoRayPickAction rpaction(this->getViewportRegion());
	rpaction.setPoint(ev->getPosition());
	rpaction.setRadius(2);
	rpaction.apply(getSceneGraph());

	SoPickedPoint * picked = rpaction.getPickedPoint();
	if(picked){
	  SbVec3f dir = picked->getPoint() - cam->position.getValue();
	  dir.normalize();
	  cam->position.setValue(picked->getPoint() - 2*dir);
	}
	return SbBool(true);
      }
      else if( type.isDerivedFrom(SoLocation2Event::getClassTypeId()) ){
	printf("process location2\n");
	SbVec2s pos = ev->getPosition();
	if (pos == ctr) return inherited::processSoEvent(ev);
	
	float dx = ctr[0] - pos[0];
	float dy = ctr[1] - pos[1];
	float len = 40*sqrt(dx*dx + dy*dy);
	if(len == 0) return inherited::processSoEvent(ev);
	dx /= -len;
	dy /= len;
	
	//     printf("dx: %f dy: %f\n",dx,dy);
	SbVec3f xi(0,sinf(0.5*dx),0);
	SbVec3f yi(0,0,sinf(0.5*dy));
	
	SbRotation rx(cosf(0.5*dx),xi[0],xi[1],xi[2]);
	SbRotation ry(cosf(0.5*dy),yi[0],yi[1],yi[2]);
	cam->orientation = ry*rx*cam->orientation.getValue();
	
	
	ignore=true;
       	QCursor::setPos(
			getGLWidget()->mapToGlobal(
						   QPoint(ctr[0],ctr[1])));
      }
    }
      

    if(isViewing())
      return SbBool(true);
    else
      return inherited::processSoEvent(ev);
}

void SoQtWorldViewer::sizeChanged(const SbVec2s & sz){
    ignore=false;
    ctr[0] = sz[0]/2;
    ctr[1] = (sz[1])/2;
    inherited::sizeChanged(sz);
  }
