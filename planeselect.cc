/* Copyright 2014 Thomas Flynn */
/*This file is part of manseg.

    manseg is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    manseg is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with manseg.  If not, see <http://www.gnu.org/licenses/>.*/

#include "planeselect.h"

SO_NODE_SOURCE(planeSelect);

SbVec3f center(vector<SbVec3f> & _pts){
  int npts = _pts.size();
  SbVec3f avg(0,0,0);
  int i;
  for(i=0;i<npts;i++)
    avg = avg + _pts[i];

  avg *= 1./npts;

  for(i=0;i<npts;i++)
    _pts[i] = _pts[i] - avg;

  return avg;
}

SbMatrix cov(vector<SbVec3f> & _pts){
  SbMatrix ret = SbMatrix::identity();
  int i,j,k,npts = _pts.size();

  for(i=0;i<npts;i++)
    for(j=0;j<3;j++)
      for(k=0;k<3;k++)
	ret[j][k] += _pts[i][j] * _pts[i][k];
  
  return ret;
}

void planeSelect::get_cov_ctr(vector<SbVec3f> & pts, SbVec3f & ctr,
			      Vector3cf  & eval, Matrix3cf  & evec){
  Matrix3f m;
  ctr = center(pts);
  SbMatrix mtx = cov(pts) ;
  
  for(int i=0;i<3;i++)
    for(int j=0;j<3;j++)
      m(i,j) = mtx[i][j];

  m *= 1.0/pts.size(); //ROWFEATS

  EigenSolver3f es(m,true);
  eval = es.eigenvalues();
  evec = es.eigenvectors();
}

void planeSelect::stopSelect(void *data,SoSelection * sel){
  planeSelect * ps = (planeSelect *) data;
  if(ps->modeFindPlane == true){

    if(ps->pts.size() == 0){
      printf("No points chosen\n");
      return;
    }
    int i,j;
    /*
    /////
    SbVec3f ctr = center(ps->pts);
    SbMatrix mtx =  cov(ps->pts) ;
   
    
    Matrix3f m;
    for(i=0;i<3;i++)
      for(j=0;j<3;j++)
	m(i,j) = mtx[i][j];
 
    EigenSolver3f es(m,true);
    Vector3cf eval = es.eigenvalues();
    Matrix3cf evec = es.eigenvectors();
    */
    SbVec3f ctr;
    Vector3cf eval;
    Matrix3cf evec;
    get_cov_ctr(ps->pts,ctr,eval,evec);

    SbVec3f nrml;
    int idx = bestIdx(eval);
    printf("EV: %f\n",std::real(eval[idx]));
    /////
    for(i=0;i<3;i++)
      nrml[i] = abs(evec(idx,i));
    
    ps->plane = SbPlane(nrml,ctr);
	
    ps->modeFindPlane = false;
    
    if(ctr.dot(nrml) > 0.) nrml *= -1.;
   
    SoMaterial * som = new SoMaterial;
    som->transparency.setValue(0.5);
    ps->children->append(som);
    ps->arrow = new SoArrow(ctr,ctr+nrml,0.1);
    ps->children->append(ps->arrow);
    ps->removeFinishCallback(stopSelect,ps);
    ps->setPointFilterCallback(NULL,NULL);

    (*ps->done)(ps->data);
  }
}

int planeSelect::bestIdx(Vector3cf v){
  if (std::abs(v[0]) <= std::abs(v[1]) && std::abs(v[0]) <= std::abs(v[2]))
    return 0;
  if(std::abs(v[1]) <= std::abs(v[2]) && std::abs(v[1]) <= std::abs(v[0]))
    return 1;
  return 2;
}

SoChildList * planeSelect::getChildren(){
  return children;
}

void planeSelect::initClass(){
  SO_NODE_INIT_CLASS(planeSelect, SoExtSelection, "ExtSelection");
}

planeSelect::planeSelect(void *){
  SO_NODE_CONSTRUCTOR(planeSelect);
  invert = false;
  lassoType = SoExtSelection::LASSO;
  lassoPolicy = SoExtSelection::PART;

  addFinishCallback(stopSelect, (void *) this);
  setPointFilterCallback(pointCB,(void *) this);
  children = new SoChildList(this,1);
  modeFindPlane = true;
  dist = 0.1;
  arrow = NULL;
}

void planeSelect::setDist(float _dist){
  dist = _dist;
  if(arrow)
    arrow->cylLength.setValue( dist.getValue() );
}

float planeSelect::getDist(SbVec3f & pt){
  return SbAbs(plane.getDistance(pt));
}

SbBool planeSelect::validPt(SbVec3f & pt){
  float pdist = plane.getDistance(pt);
  float d = dist.getValue();
  return (invert) ? SbAbs(pdist) > d : SbAbs(pdist) < d;
}


SbBool planeSelect::pointCB( void *pData, SoCallbackAction * pAction, 
			     const SoPrimitiveVertex *pVertex ){

  planeSelect * ps = (planeSelect *)pData;
  ps->pts.push_back(pVertex->getPoint());
  //check ignore?
  SoPointDetail *pDetail = (SoPointDetail*)pVertex->getDetail();
  int coordIndex = pDetail->getCoordinateIndex();
  printf("%d\n",ps->pickCBData);
  return FALSE;
}

