/* Copyright 2014 Thomas Flynn */
/*This file is part of manseg.

    manseg is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    manseg is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with manseg.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef _PLANESELECT
#define _PLANESELECT

#include <Inventor/nodes/SoSubNode.h>
#include <Inventor/nodes/SoExtSelection.h>
#include <Inventor/fields/SoSFEnum.h>
#include <Inventor/SbLinear.h>
#include <Inventor/SoPrimitiveVertex.h>
#include <Inventor/misc/SoChildList.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/details/SoPointDetail.h>
#include <Inventor/fields/SoSFFloat.h>
#include "SoArrow.h"

#include <vector>

#include "Eigen/Eigen"

using namespace Eigen;
using namespace std;


typedef EigenSolver<Matrix3f> EigenSolver3f;

typedef void DoneCB(void * _bs);

class planeSelect : public SoExtSelection {

  SO_NODE_HEADER(planeSelect);

  SbPlane plane;
  vector<SbVec3f> pts;
  bool modeFindPlane;
  //float dist;
  SoSFFloat dist;
  SoChildList * children;
  static int bestIdx(Vector3cf);
public:
  SoArrow * arrow;
  bool invert;

  void setDone(DoneCB * _done, void *_data){ 
    done = _done;
    data = _data;
  }

  void setDist(float _dist);
  SbBool validPt(SbVec3f &);
  float getDist(SbVec3f &);
  planeSelect(void * _cw = NULL);
  static void initClass();
  static SbBool pointCB( void * pData, SoCallbackAction * pAction,
			 const SoPrimitiveVertex *pVertex);
  static void stopSelect(void *, SoSelection *);
  virtual SoChildList * getChildren();
  void * data;
  DoneCB * done;

  static void get_cov_ctr(vector<SbVec3f> & pts,  SbVec3f & ctr, 
			  Vector3cf & eval, Matrix3cf & evec);

};

#endif
