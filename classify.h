/* Copyright 2013 Thomas Flynn */
/*This file is part of manseg.

    manseg is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    manseg is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with manseg.  If not, see <http://www.gnu.org/licenses/>.*/

#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>

using namespace std;

#ifndef CLASS_CLASSIFY
#define CLASS_CLASSIFY

class classify{
 public:

  void loadClasses(const string & fn);
  classify(const string & fn);
  classify();

  void addClass(string name, unsigned char color[3], int id);
  void addClass(char * name,unsigned char color[3], int id);
  void addClass(char * name,unsigned char r, unsigned char g, unsigned char b, int id);

  void getColorByName(string name, unsigned char * ret);
  void getColorByName(string name, float * ret);
  void getColorByIdx(int idx, unsigned char &r, unsigned char &g, unsigned char &b);
  void getColorByIdx(int idx, float *ret);

  int getIdByName(string name);
  int getIdByIdx(int idx);

  int getIdxByName(string name);
  int getIdxById(int id);

  void getClassByIdx(int idx, string &name, 
		     unsigned char &r, unsigned char &g, unsigned char &b);
  void getClassByIdx(int idx,string &name, float &r, float &g, float &b);

  int getNClasses() { return classNames.size(); }
  string getNameByIdx(int idx);

 private:
  vector<string> classNames;
  vector<unsigned char *> classColors;
  vector<int> classIds;
};

#endif
