
/*-----------------------------HEPVis----------------------------------------*/
/*                                                                           */
/* Node:             SoArrow                                                 */
/* Description:      Represents an Arrow, or Vector or something             */
/* Author:           Joe Boudreau September 1997                             */
/*                                                                           */
/*---------------------------------------------------------------------------*/

/* modified by Thomas Flynn 2012 */

#include <assert.h>
#include <math.h>
#include <Inventor/SbBox.h>
#include <Inventor/actions/SoGLRenderAction.h>
#include <Inventor/fields/SoSFFloat.h>
#include <Inventor/misc/SoChildList.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoRotation.h>
#include <Inventor/nodes/SoRotationXYZ.h>
#include <Inventor/nodes/SoCylinder.h>
#include <Inventor/nodes/SoCone.h>
#include <Inventor/nodes/SoScale.h>
#include <Inventor/nodes/SoDrawStyle.h>

//#include <HEPVis/SbMath.h>
#include "SoArrow.h"

// This statement is required
SO_NODE_SOURCE(SoArrow)

// Constructor
SoArrow::SoArrow(){
  tip[0] = tip[1] = tip[2] = 1.0;
  tail[0] = tail[1] = tail[2] = 0.0;
  size = 0.1;
  updateBBox();
  SO_NODE_CONSTRUCTOR(SoArrow);
  SO_NODE_ADD_FIELD(alternateRep, (NULL));
  SO_NODE_ADD_FIELD(cylLength, (1.0) );

  children = new SoChildList(this);
}

SoArrow::SoArrow(SbVec3f _tail, SbVec3f _tip, float _size) {
  // This statement is required
  SO_NODE_CONSTRUCTOR(SoArrow);

  // Data fields are initialized like this:
  /*  SO_NODE_ADD_FIELD(tail,                (0,0,0));
  SO_NODE_ADD_FIELD(tip,                 (0,0,1));
  SO_NODE_ADD_FIELD(size,                (0.02F) );*/
  tip = _tip;
  tail = _tail;
  size = _size;
  SO_NODE_ADD_FIELD(alternateRep,        (NULL));

  SO_NODE_ADD_FIELD(cylLength, (size) );

  children = new SoChildList(this);
  generateChildren();
  updateChildren();

  updateBBox();
 
}

// Destructor
SoArrow::~SoArrow() {
  delete children;
}


// initClass
void SoArrow::initClass(){
  // This statement is required.
  SO_NODE_INIT_CLASS(SoArrow,SoShape,"Shape");
}


// GLRrender
void SoArrow::GLRender(SoGLRenderAction *action) {
 if (!shouldGLRender(action)) return;
 updateChildren();
 children->traverse(action);  
}

// generatePrimitives
void SoArrow::generatePrimitives(SoAction *action) {

  children->traverse(action);
}

// getChildren
SoChildList *SoArrow::getChildren() const {
  return children;
}

void SoArrow::updateBBox(){
 
  SbVec3f mtip=tip,
          mtail=tail;

  float size=(mtip-mtail).length();
  
  float xmin,ymin,zmin,xmax,ymax,zmax;

  if (mtip[0]>mtail[0]) {
    xmax =mtip [0]+0.2F*size;
    xmin =mtail[0]-0.2F*size;
  }
  else {
    xmax =mtail[0]+0.2F*size;
    xmin =mtip [0]-0.2F*size;
  }

  if (mtip[1]>mtail[1]) {
    ymax =mtip [1]+0.2F*size;
    ymin =mtail[1]-0.2F*size;
  }
  else {
    ymax =mtail[1]+0.2F*size;
    ymin =mtip [1]-0.2F*size;
  }

  if (mtip[2]>mtail[2]) {
    zmax =mtip [2]+0.2F*size;
    zmin =mtail[2]-0.2F*size;
  }
  else {
    zmax =mtail[2]+0.2F*size;
    zmin =mtip [2]-0.2F*size;
  }

  SbVec3f MMin(xmin,ymin,zmin), MMax(xmax,ymax,zmax);
  _center.setValue((xmax+xmin)/2,(ymax+ymin)/2,(zmax+zmin)/2);
  _box.setBounds(MMin,MMax);


}

// computeBBox
void SoArrow::computeBBox(SoAction *, SbBox3f &box, SbVec3f &center ){

  box = _box;
  center = _center;
  printf("bb\n");
  
}


// updateChildren
void SoArrow::updateChildren() {
  assert(children->getLength()==1);
  if( cachedTip != tip || cachedTail != tail || cachedCylLength != cylLength ) {
  printf("uc\n");
    // Redraw the Arrow....
    
    SoSeparator       *axis                = (SoSeparator *)  ( *children)[0];
    SoTranslation     *finalTranslation    = (SoTranslation *)( axis->getChild(0));
    SoScale           *scale               = (SoScale *      )( axis->getChild(1));
    SoRotation        *myRotation          = (SoRotation *   )( axis->getChild(2));
    SoRotationXYZ     *rot                 = (SoRotationXYZ *)( axis->getChild(3));
    SoTranslation     *zTranslation        = (SoTranslation *)( axis->getChild(4));
    SoTranslation     *cTranslation        = (SoTranslation *)( axis->getChild(6));
    SoCone            *axisCone            = (SoCone *       )( axis->getChild(7));
    SoTranslation *dTranslation = (SoTranslation *)(axis->getChild(8));
    SoDrawStyle * ds                       = (SoDrawStyle   *)( axis->getChild(9));
    SoCylinder * distCyl                   = (SoCylinder    *)( axis->getChild(10));

    double length = (tip-tail).length();
    finalTranslation->translation.setValue(tail);
    /* scale->scaleFactor.setValue((float)(length/2),
                                (float)(length/2),
                                (float)(length/2));*/
    printf("length: %f\n",length);
    SbVec3f ax = SbVec3f(0,0,1).cross(tip-tail);
    double  an = asin(ax.length()/length);
    myRotation->rotation.setValue(ax,(float)an);
    zTranslation->translation.setValue(0,1,0);
    rot->axis=SoRotationXYZ::X;
    rot->angle=(float)(M_PI/2.0);

    float arrowsize = size;
  
    SoCylinder* axisCyl  = (SoCylinder *   )( axis->getChild(5));
    axisCyl->radius.setValue( 2 * arrowsize );
   
    cTranslation->translation.setValue(0,1,0);
    axisCone->bottomRadius.setValue( arrowsize * 4.0F );
    axisCone->height.setValue( arrowsize * 8.0F );

    dTranslation->translation.setValue(0,-2.,0);
    ds->style =  SoDrawStyle::LINES;
    distCyl->radius.setValue(4 * arrowsize);
    distCyl->height.setValue(cylLength.getValue() * 2);
  }
  cachedCylLength = cylLength;
  cachedTip=tip;
  cachedTail=tail;
}

// generateChildren
void SoArrow::generateChildren() {
  assert(children->getLength() ==0);
  SoSeparator   *axis=new SoSeparator();
  SoTranslation *finalTranslation=new SoTranslation;
  SoScale       *scale=new SoScale();
  SoRotation *myRotation = new SoRotation();
  SoRotationXYZ  *rot = new SoRotationXYZ;
  SoTranslation  *zTranslation=new SoTranslation;
  SoTranslation  *cTranslation=new SoTranslation;
  SoCone     *axisCone=new SoCone;
  SoCylinder* axisCyl = new SoCylinder;

  SoTranslation * dTranslation = new SoTranslation;
  SoDrawStyle * ds = new SoDrawStyle;
  
  SoCylinder * distCyl = new SoCylinder;
  
  axis->addChild(finalTranslation);
  axis->addChild(scale);
  axis->addChild(myRotation);
  axis->addChild(rot);
  axis->addChild(zTranslation);

 
  axis->addChild( axisCyl );

  
  axis->addChild(cTranslation);
  axis->addChild(axisCone);
  axis->addChild(dTranslation);
  axis->addChild(ds);
  axis->addChild(distCyl);

  children->append(axis);
}

// generateAlternateRep
void SoArrow::generateAlternateRep() {
 // This routine sets the alternate representation to the child
  // list of this mode.  


  alternateRep.setValue((SoSeparator *)  ( *children)[0]);
}

// clearAlternateRep
void SoArrow::clearAlternateRep() {
  alternateRep.setValue(NULL);
}

