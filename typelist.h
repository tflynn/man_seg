/* Copyright 2013 Thomas Flynn */
/*This file is part of manseg.

    manseg is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    manseg is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with manseg.  If not, see <http://www.gnu.org/licenses/>.*/

#include <QWidget>
#include <QListWidget>
#include <QListWidgetItem>
#include <QVBoxLayout>
#include <QGroupBox>
#include <iostream>
#include "classify.h"
#include <stdio.h>
using namespace std;

#ifndef TYPELIST
#define TYPELIST

class TypeList : public QWidget
{
  Q_OBJECT
 public:
  TypeList(classify * _cl, QWidget * parent = 0);
  int getType();
  bool badType(int i);
  private slots:
  void itemClicked(QListWidgetItem *);
  void itemChanged(QListWidgetItem *);
 private:
  int type;
  classify * cl;
  QListWidget * _list;
  vector<bool> ignlist;
};

#endif
