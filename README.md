man_seg is a program for manual labeling of 3D point cloud data. Think MS paint for point clouds.
It can be used for generating ground truth data and for comparing two labelings of a 3D scene (for instance comparing
ground truth data with inferred labels)

![manseg.png](https://bitbucket.org/repo/rjRgMa/images/421471449-manseg.png)


## Requirements ##

* [Coin3D](https://bitbucket.org/Coin3D/coin/wiki/Home) (Version 3.1.3 or newer)
* [SoQt](https://bitbucket.org/Coin3D/soqt)
* Qt (Qt 4 or newer)

This program has been developed and tested in linux

## Compilation ##

The compilation requires access to some header files from the Coin source tree. Update the two INCLUDEPATH directives in man_seg.pro to point to your local copy of the Coin source code. Then use the two commands

qmake

make

## Running ##

To run the program you need a point cloud file (currently the program supports the ptx format), a label file which maps class names to label colors, and optionally a label file. A file 'classes' which defines several common object types is included. To start labelling a file 'mypts.ptx' type

./man_seg mypts.ptx classes

If you save the labels as mylbls.lbl you can start where you left off by typing

./man_seg mypts.ptx classes mylbls.lbl

## Credits ##
This software was developed at the Computer Vision and Graphics Lab of Hunter College. This work has been supported in part by the following NSF grants: IIS-0915971, CCF-0916452 and MRI CNS-0821384