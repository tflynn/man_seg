/* Copyright 2013 Thomas Flynn */
/*This file is part of manseg.

    manseg is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    manseg is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with manseg.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef _BRUSHSELECT
#define _BRUSHSELECT

#include <Inventor/nodes/SoSubNode.h>
#include <Inventor/nodes/SoSelection.h>
#include <Inventor/nodes/SoExtSelection.h>
#include <Inventor/fields/SoSFEnum.h>
#include <Inventor/events/SoMouseButtonEvent.h>
#include <Inventor/events/SoLocation2Event.h>
#include <Inventor/events/SoEvent.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/actions/SoRayPickAction.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>

class CoinWindow;

class brushSelect : public SoSelection {

  SO_NODE_HEADER(brushSelect);
  CoinWindow * cw;
  bool down;
  SoRayPickAction * raypick;
  SoNode * root;
  SoChildList * children;
  
public:
  ~brushSelect(){
    printf("DEST\n");
  }

  SoSFFloat  rad;
  void setRad(int _rad);
  virtual void GLRender(SoGLRenderAction *action);
  brushSelect(CoinWindow * _cw = NULL);
  static void initClass();
 
  static void mouseButtonCB(void * data, SoEventCallback * eventCB);
  static void location2CB(void *data, SoEventCallback * eventCB);
  virtual SoChildList * getChildren();
  virtual void handleEvent(SoHandleEventAction *action);

};

#endif
